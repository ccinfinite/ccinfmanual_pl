=====================
cc-infinite
=====================

**Computing Competences.
Innovative learning approach for non-IT students.**

.. figure:: img/ccinflogo.png
  :scale: 50

CC Infinite jest skierowany do wszystkich zainteresowanych nauką programowania. Nie chcemy ograniczać tej umiejętności tylko do studentów informatyki lub entuzjastów IT. Wierzymy, że każda osoba, niekoniecznie związana z branżą IT, może uprzyjemnić sobie pracę z komputerem i przyspieszyć pracę znając zasady programowania. Głównym celem projektu będzie zwiększenie innowacyjności i interdyscyplinarności szkolnictwa wyższego poprzez opracowanie kursu programowania dla studentów kierunków innych niż informatyka i ocenę jego efektywności na 4 europejskich uniwersytetach.

Niniejsza manual powstał w wyniku `projektu CC Infinite <https://ccinfinite.smcebi.edu.pl>`_ współfinansowany w ramach programu Unii Europejskiej `Erasmus+ <https://www.erasmusplus.eu/>`_.

#################################
Interaktywny kurs programowania
#################################

.. toctree::
   :maxdepth: 1

   main/pl_main_01_intro.rst
   main/pl_main_02_variables.rst
   main/pl_main_03_strings.rst
   main/pl_main_04_io.rst
   main/pl_main_05_collections.rst
   main/pl_main_06_conditions.rst
   main/pl_main_07_while_loop.rst
   main/pl_main_08_for_loop.rst
   main/pl_main_09_functions.rst
   main/pl_main_10_functions.rst
   main/pl_main_11_module.rst
   main/pl_main_12_oop.rst


Oferowane przez nas materiały dydaktyczne zostały opracowane w ramach międzynarodowego konsorcjum.

.. figure:: img/uni.png
   :scale: 80

.. figure:: img/ccdisclaimer.png
   :scale: 80
