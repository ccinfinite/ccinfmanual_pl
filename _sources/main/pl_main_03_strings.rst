.. _znaki:

.. Typy sekwencyjne
.. ================
..
.. .. index:: typy sekwencyjne, typ sekwencyjny
..
.. Do typów sekwencyjnych zaliczymy listy (``list``), krotki (``tuple``),
.. łańcuchy znaków (``str``).  Innymi typami zmiennych po których można iterować,
.. ale nie zaliczamy ich do sekwencji, są słowniki (``dict``) oraz zbiory (``set``).

******************
Łańcuchy znaków
******************

.. index:: łańcuch znaków, ciąg znaków, str, indeks

Łańcuch (ciąg) znaków tworzymy przy pomocy pary apostrofów ``'...'`` lub cudzysłowów ``"..."`` pomiędzy którymi możecie wpisać dowolne znaki jakie znajdziecie na klawiaturze, a nawet takie jakich na niej nie znajdziecie.
W języku Python typ który reprezentuje ciąg znaków nazywa się ``str``.

Na łańcuch ``'Mielonka i jajka'`` możemy spojrzeć jak na
zbiór znaków, gdzie pierwszym znakiem jest ``M``, drugim ``i``, trzecim ``e``
i tak dalej. Możemy więc ponumerować znaki w takim łańcuchu. W języku Python
przyjęło się numerować od liczby ``0``, zatem pierwszemu znakowi z łańcucha odowiada liczba ``0``, a ostatniemu znakowi liczba ``długość_ciągu_znaków - 1``. W przypadku naszego ciągu literze ``M``
odpowiadać będzie liczba ``0``, literze ``i`` liczba ``1``, literze ``e``
liczba ``2``, itd.  Owe liczby (0, 1, 2...) nazywamy **indeksami**.
Indeksy służą do wskazywania konkretnego miejsca w łańcuchu znaków. Wskazując na konkretne miejsce, np: piąte, mówimy, że odwołujemy się do znaku o indeksie 5. Będzie to oczywiście szósty znak z łańcucha (u nas literka ``n``).
Odwołanie takie wykonuje się za pomocą nawiasów kwadratowych:

.. code-block:: python

    ciag_znakow[indeks]

Na bazie powyższego łańcucha znaków odwołamy się do indeksów ``2`` i ``5``,
czyli odpowiednio do trzeciego i szóstego miejsca w łańcuchu. Zobaczmy, jak
to działa w *ActiveCode*

.. activecode:: pl_main_0301
    :caption: indeksowanie sekwencji

    tekst = 'Mielonka i jajka'
    print('(e) tekst[2] =', tekst[2])
    print('(n) tekst[5] =', tekst[5])


.. index:: len, długość sekwencji

Ostatnim indeksem w zmiennej ``tekst`` jest liczba 15. Jest to liczba równa
``długość_ciągu_znaków - 1``.  Aby obliczyć długość sekwencji możemy posłuzyć
się funkcją ``len('jakiś tekst')``, co oznacza, że ostatni dostępny indeks sekwencji ``seq`` dany jest wzorem ``len(seq) - 1`` i właśnie za pomocą takiego
wyrażenia możemy odwołać się do ostatniego elementu sekwencji.

.. code-block:: python

    seq[len(seq) - 1]

Drugim od końca elementem będzie oczywiście ``seq[len(seq) - 2]`` itd.
Wypisywanie (i obliczanie) za każdym razem długości sekwencji nie jest
szczególnie korzystne i Python umożliwia nam odwoływanie się do sekwencji *od
końca* po prostu za pomocą ujemnych indeksów.

.. activecode:: pl_main_0302
    :caption: indeksowanie sekwencji za pomocą ujemnych indeksów

    tekst = 'Mielonka i jajka'
    print('(ostatnie a) tekst[15] =', tekst[15])
    print('(ostatnie a) tekst[len(tekst) - 1] =', tekst[len(tekst) - 1])
    print('(ostatnie a) tekst[-1] =', tekst[-1])
    print('(ostatnie k) tekst[-2] =', tekst[-2])
    print('(M) tekst[-16] =', tekst[-16])


Poniższa tabela podsumowuje możliwości odwoływań do poszczególnych miejsc w
sekwencji ``s = "Python"``.

=============== ========== ========== ========== ========== ==========
P               y          t          h          o          n
=============== ========== ========== ========== ========== ==========
0               1          2          3          4          5
len(s) - len(s) len(s) - 5 len(s) - 4 len(s) - 3 len(s) - 2 len(s) - 1
-len(s)         -5         -4         -3         -2         -1
=============== ========== ========== ========== ========== ==========

Operacje na łańcuchach
~~~~~~~~~~~~~~~~~~~~~~

.. index:: konkatenacja

Na zmiennych prostych, takich jak liczby, możemy wykonywać różne operacje. Możemy je dodawać, odejmować... w sumie mówiliśmy o tym w :ref:`poprzednim rozdziale <zmienne>`. Nie inaczej jest z łańcuchami. Możemy takie łańcuchy do siebie dodawać za pomocą operatora ``+``

.. code-block:: python

    >>> 'Mielonka' + 'jajka'
    'Mielonkajajka'

Możemy też je mnożyć przez liczbę (choć tylko typu ``int``).

.. code-block:: python

    >>> 'Mielonka' * 3
    'MielonkaMielonkaMielonka'

Spróbujcie sami

.. activecode:: pl_main_0303
    :caption: proste operacje na łańcuchach

    print('Mielonka' + 'jajka')
    print('Mielonka' * 3)

Wszystko działa tak samo, gdy najpierw przechwycimy napis do zmiennej

.. .. showeval:: showEval_concat_multi
..    :trace_mode: true
..
..    tekst = 'Mielonka'
..    inny_tekst = 'jajka'
..    ~~~~
..    {{'Mielonka' + 'jajka'}}{{tekst + inny_tekst}}
..    {{'Mielonka' * 3}}{{tekst * 3}}


.. activecode:: pl_main_0304
    :caption: proste operacje na zmiennych łańcuchowych

    tekst = 'Mielonka'
    inny_tekst = 'jajka'
    print(tekst + inny_tekst)
    print(tekst * 3)


Spójrz na krótki kod poniżej.

.. code-block:: python
   :linenos:

   tekst = 'Kup jajko'
   wynik = (tekst[5] + tekst[6]) * 3

.. fillintheblank:: fitb-pl_main_03ex1

  Jaka wartość przypisze się do zmiennej ``wynik``?

  - :'|"ajajaj'|": Poprawnie.
    :'aj': Na końcu jest jeszcze mnożenie przez 3.
    :"aj": Na końcu jest jeszcze mnożenie przez 3.
    :' a a a': W języku Python indeksujemy od liczby 0.
    :x: Niestety nie. Przyjżyjcie się liczbom w nawiasach kwadratowch - do jakich liter odnoszą się te liczby? Sklejcie te literki i nie zapomnijcie o pomnożeniu przez 3. Może nie uwzględniliście apostrofów (lub cudzysłowów) w odpowiedzi?



Łańcuchy możemy porównywać ze sobą. Jeżeli chcemy zapytać, czy dwa ciągi znaków są takie same (składają się z tych samych liter) to możemy zapytać

.. activecode:: pl_main_0305
    :caption: porównanie łańcuchów

    tekst = 'Mielonka'
    ten_sam_tekst = 'Mielonka'
    inny_tekst = 'jajka'
    print('Czy Mielonka to Mielonka?', tekst == ten_sam_tekst)
    print('Czy Mielonka to nie jajka?', tekst != inny_tekst)

Jeżeli chodzi o pozostałe operacje porównania to sprawa jest nieco bardziej skomplikowana. W tym przypadku Python porównuje znaki element po elemencie. Dla pary znaków porównuje wartości jakim odpowiadają znaki z danego ciągu w tablicy znaków (ASCII, Unicode...). Jeżeli w kolejnej parze znaków wartości się różnią, to większy jest ten ciąg znaków, którego wartość tablicy znaków jest większa. Więcej o porównaniach ciągów znaków dowiecie się w części traktującej o :ref:`manipulacji stringami <pl_lingua_intro>`.


.. _wycinanie_sekwencji:

Wycinanie sekwencji
~~~~~~~~~~~~~~~~~~~

Za pomocą indeksów możemy też odwołać się do większej części sekwencji. Służy
do tego technika wycinania sekwencji (*sequence slicing*). Ogólna postać
wycinania wygląda następująco:

.. code-block:: python

    seq[start:stop:krok]

Jak widać musimy w ogólności podać 3 liczby:

* ``start`` - indeks, od którego mamy zacząć wycinać
* ``stop`` - indeks, na którym wcięcie ma się zakończyć, wartość stojąca pod
  tym indeksem jest pomijana
* ``krok`` - co który indeks ma być brany pod uwagę przy wycinaniu

Zerknijmy na prosty przykład. Proszę zauważyć, że do argumnetów ``start``,
``stop`` i ``krok`` możemy podstawiać liczby zarówno dodatnie, jak i ujemne.
Zero oczywiście też.

.. activecode:: pl_main_0306
    :caption: wycinanie sekwencji

    tekst = 'Mielonka i jajka'
    print('(elo) od 2 do 5 co 1:', tekst[2:5:1])
    print('(n j) od 5 do 12 do 3:', tekst[5:12:3])
    print('(Meok  ak) od poczatku do konca co 2', tekst[0:len(tekst):2])
    print('(akjaj i ak) do konca do 5 z ujemnym krokiem', tekst[-1:5:-1])

Specjalnego typu krokiem jest ``1``. Jeżeli ``krok = 1`` oznacza to, że mamy
na myśli każdy kolejny indeks z żądanego zakresu. Jedynka jest wartością
domyślną i możemy ją pominąć.

.. code-block:: python

    >>> tekst[2:5] == tekst[2:5:1]
    True

Specjalnymi indeksami są ``0`` oraz ``-1`` (``len(seq) - 1``). Oznaczają one
początek i koniec sekwencji. Miejsca te są dla interpretera oczywiste, dlatego
i je możemy pomijać, wycinając sekwencje.

.. activecode:: pl_main_0307
    :caption: pomijanie specjalnych indeksów przy wycinaniu

    tekst = 'Mielonka i jajka'
    print('od poczatku do 5 co 1:', tekst[:5])
    print('od 8 do konca co 2:', tekst[8::2])
    print('kopia sekwencji (od poczatku do konca co 1)', tekst[:])
    print('odwracanie sekwencji', tekst[::-1])

Podobne odwołania można wykonywać na sekwencjach dowolnego typu.

Wracamy do poprzedniego zadania. Tym razem chcemy, korzystając ze zmiennej ``tekst`` przypisać do wyniku ``'ajajaj'`` ale za pomocą powyższej konstrukcji wycinania łańuchów.

.. code-block:: python
   :linenos:

   tekst = 'Kup jajko'
   wynik = tekst[start:stop] * 3

.. fillintheblank:: fitb-pl_main_03ex2

  Jak będzie wyglądał kod, tak by w zmiennej wynik znalazł się ciąg ``ajajaj``?

  wynik = tekst[|blank|:|blank|] * 3

  - :5: Poprawnie.
    :6: Język Python numeruje pozycje w łańcuchu od zera.
    :x: Niestety nie.
  - :7: Poprawnie.
    :8: Język Python numeruje pozycje w łańcuchu od zera.
    :6: Nie do końca. Pamiętaj, że druga wartość nie jest uwzględniana w wycinaniu.
    :x: Niestety nie.


.. index:: operator in, operator not in, in, not in

Operatory zawierania elementu w sekwencji
-----------------------------------------

Dla sekwencji istnieją specjalne operatory zawierania się danego elementu w
zbiorze, sekwencji.  Operator ``in`` sprawdza czy jakiś element znajduje się w sekwencji, a operator ``not in`` sprawdza, czy elementu nie ma.

.. activecode:: pl_main_0308
    :caption: Operatory in oraz not in

    tekst = 'Mielonka i jaja'
    print('Jest M w tekscie?', 'M' in tekst)
    print('Jest z w tekscie?', 'z' not in tekst)

Jak widać wynik działania tych operatorów jest wartością logiczną - prawdą lub fałszem.

.. _konkatenacja_znakow:

.. index:: konkatenacja, funkcja str

Sklejanie łańcuchów znaków i funkcja ``str``
--------------------------------------------

Na chwilę wrócimy do sklejania łańcuchów. W przykładzie powyżej, używaliśmy operatora ``+`` aby dodać do siebie dwa znaki. W przypadku łańcuchów znaków możemy w ten sposób sklejać dowolne ciągi znaków. Takie sklejanie sekwencji będziemy nazywać **konkatenacją**. W ten sposób powstaje nowy ciąg znaków.

.. activecode:: pl_main_0309
    :caption: Konkatenacja

    print('Ala' + 'ma' + 'kota')
    zmienna = 'Ala ma kota.' + ' ' + 'Kot ma Alę.'
    print(zmienna)

Jeżeli chcielibyśmy w taki sposób wygenerować ciąg znaków sklejając ze sobą napis oraz liczbę (rozumianą jako na przykład typ ``int``), to dodając do siebie takie wielkości zobaczymy, że dostajemy błąd typu.

.. index:: rzutowanie, funkcja str

.. code-block:: python

    >>> ilosc_kotow = 2
    >>> 'Ala ma ' + ilosc_kotow + ' koty'
    TypeError: cannot concatenate 'str' and 'int' objects

W pewnym sensie moglibysmy sie tego spodziewać, bo operator ``+`` oznacza co innego dla liczb i co innego dla łańcuchów. Aby móc *dynamicznie* utworzyć napis za pomocą operatora ``+`` oraz zmiennej liczbowej ``ilosc_kotow``, w którym będzie stało, że ``'Ala ma 2 koty'``, będziemy musieli przeprowadzić operację zamiany liczby na typ ``str``. Operację taką nazywamy **rzutowaniem** typu ``int`` na typ ``str`` i do jej przeprowadzenia wykorzystujemy funkcję o nazwie rządanego typu - tutaj będzie to ``str``. Wystarczy *opakować* interesującą nas zmienną w funkcję ``str(ilosc_kotow)``. Funkcja ta zwróci reprezenatcję danej zmiennej w typie ``str``.

.. activecode:: pl_main_0310
    :caption: Konkatenacja, funkcja rzutująca str

    ilosc_kotow = 2
    print('Ala ma ' + str(ilosc_kotow) + ' koty')

Większość wbudowanych typów posiada taką reprezentację, możemy zatem doklejać do łańcuchów znaków dowolne inne literały. Poniżej jest przykład z listą, ale wypróbujcie też inne!

.. activecode:: pl_main_0311
    :caption: Funkcja rzutująca str

    fib_lista = [1, 1, 2, 3, 5, 8, 13, 21]
    fib_info = '8 wyrazów ciągu Fibonacciego to ' + str(fib_lista)
    print(fib_info)

Metoda ``format``
=================
Dla ciągów znaków istnieje wbudowana metoda o nazwie ``format``, która umożliwia budowanie łańcuchów w miarę dynamicznie, bez konieczności używania konkatenacji i funkcji rzutującej, co w przypadku nieco bardziej skomplikowanych łańcuchów może być mocno karkołomne. Z metodą ``format`` jest zdecydowanie prościej.

Jeżeli mamy trzy zmienne o nazwach ``a, b, c``, to chcąc je wkleić po kolei do ciągu znaków za pomocą funkcji ``format`` musimy napisać

.. code-block:: python

    '{}, {}, {}'.format(a, b, c)

Możemy też użyć liczb określających kolejność argumentów, poczynając od zera. Dzięki temu kolejność zmiennych w stringu może być różna od kolejności argumentów.

.. code-block:: python

    '{0}, {1}, {2}'.format(a, b, c)
    '{2}, {1}, {0}'.format(a, b, c)

Można też, dzięki temu, wykorzystać daną zmienną kilkukrotnie

.. code-block:: python

    {0}{1}{0}'.format('abra', 'kad')

Możemy też, zamiast indeksów argumentów użyć ich nazw

.. code-block:: python

    'Wspórzędne: {lat}, {long}'.format(lat='37.24N', long='-115.81W')

Możecie to wszystko teraz wypróbować. Warto używać funkcji ``format`` do formatowania ciągów znaków, gdyż daje ona dużą kontrolę nad końcowym efektem. Ta lekcja nie wyczerpuje zagadnienia ciągów znaków i ich formatowania. Jeżeli chcielibyście się dowiedzieć czegoś więcej o formatowaniu łańcuchów, polecamy `dokument PyFormat <https://pyformat.info>`_.

.. activecode:: pl_main_0312
    :caption: str.format()

    a, b, c = 'jeden', 'dwa', 'trzy'
    print('{}, {}, {}'.format(a, b, c))
    print('{0}, {1}, {2}'.format(a, b, c))
    print('{2}, {1}, {0}'.format(a, b, c))
    print('{0}{1}{0}'.format('abra', 'kad'))
    print('Wspórzędne: {latitude}, {longitude}'.format(latitude='37.24N', longitude='-115.81W'))

.. topic:: Ćwiczenie

    TBA
