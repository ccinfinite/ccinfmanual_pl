.. _petla_for:

*************
Pętla ``for``
*************

.. index:: for

Instrukcja ``for`` ta służy do iterowania (przechodzenia) po elementach sekwencji. Składnia tej pętli jest następująca:ś

.. code-block:: python

    for element in sekwencja:
        BLOK_FOR

Taka pętla będzie miała dokładnie ``len(sekwencja)`` obrotów, zatem blok
instrukcji pętli wywoła się dokładnie tyle razy, ile mamy elementów sekwencji.
Spójrzmy na przykład.

.. activecode:: pl_main_0801
    :caption: przykład sekwencji

    for el in 'Ala ma kota':
        print('element: ' + el)

Możemy też najpierw zadeklarować zmienną, a później po niej iterować:

.. activecode:: pl_main_0802
    :caption: inny przykład sekwencji

    seq = (1, 25, 100, 10000)
    for el in seq:
        print('pierwiastek z', el, "=", el ** 0.5)

Oczywiście, w miejsce zmiennej ``seq`` możemy wstawić dowolną zmienną posiadającą elementy: listę, krotkę, łańcuch znaków czy słownik. W przypadku tego ostatniego, iterować będziemy po kluczach.

.. activecode:: pl_main_0803
    :caption: słowniki i pętla for

    urodziny = {'Ania': '30 sierpnia', 'Bartek': '17 lutego', 'Celina': '2 sierpnia'}
    for imie in urodziny:
        print(imie, 'ma urodziny', urodziny[imie])


.. index:: range

Funkcja ``range``
~~~~~~~~~~~~~~~~~

Istnieje niezwykle przydatna funkcja tworzące obiekty iterowalne, które możemy wykorzystać bezpośrednio w pętli ``for``. Funkcja ``range`` tworzy sekwencję liczb całkowitych, a jej wywołanie ma następującą składnię

.. code-block:: python

    range(start, stop, krok)

i przypomina wycinanie sekwencji. Funkcja ta zwróci nam sekwencję liczb, poczynając od wartości ``start``, a kończąc na ``stop - 1`` dokładnie co ``krok``. Np.

.. activecode:: pl_main_0804

    seq = range(1, 11, 2)
    for el in seq:
        print(el)

wydrukuje liczby ``1, 3, 5, 7, 9``. Wielkości ``start`` oraz ``krok`` są
opcjonalne. Jeżeli podamy tylko jedną liczbę ``A`` do funkcji ``range(A)``
dostaniemy wszystkie liczby całkowite od ``0`` do liczby ``A - 1``. Jeżeli podamy 2 liczby, to pierwsza z nich będzie interpretowana jak ``start``, a druga jak ``stop``.  Krok możemy określić tylko podając komplet. Możliwe są oczywiście wielkości ujemne, w tym ujemny krok. Dzięki temu stworzymy listę liczb malejących. Musimy tylko pamiętać o tym, żeby wartość początkowa była większa od końcowej. Po raz stworzonej liście możemy iterować:

.. activecode:: pl_main_0805
    :caption: pętla for iterująca po liście wygenerowanej z funkcji range

    for liczba in range(200, 100, -25):
        print(liczba)

.. topic:: Ćwiczenie

    Bazując na ćwiczeniu z poprzedniej lekcji, za pomocą funkcji ``range``, instrukcji ``if`` oraz pętli ``for`` znajdź sumę wszystkich liczb podzielnych przez 5 i 7 w zakresie od 2998 do 4444 (powinieneś dostać wartość 152110).

.. index:: iterator

Funkcja ``range``, tworzy szczególny typ sekwencji, zwany
**iteratorem**. Nie będziemy teraz wchodzić w szczegóły, ale jako, że nazwę tą
często można spotkać w tutorialach czy książkach na temat języka Python, to
warto zapamiętać, że jeżeli wykorzystujemy taki iterator w pętli to wynik jego
działania będzie taki sam jak pełnej sekwencji (np. listy). Używając iteratorów
oszczędzamy pamięć, bowiem nie tworzą one pełnej listy w pamięci komputera, a w
każdym kroku iteracji obliczają kolejną wartość, pamiętając jedynie
wartość obecną, krok oraz wartość końcową, której nie mogą przekroczyć.

Funkcja ``enumerate``
~~~~~~~~~~~~~~~~~~~~~

Czasem mając do dyspozycji sekwencję, chcemy w pętli odwołać się do tych elementów, które stoją w jakimś interesującym nas miejscu, np: na miejscach nieparzystych lub tych w drugiej połowie. Możemy z powodzeniem wykorzystać do tego indeksy elementów, choć przecież pętla ``for`` iteruje po elementach nie indeksach. Aby nie używać do tego typowych rozwiązań tj. wprowadzenia dodatkowej zmiennej, która będzie grać rolę indeksu, tak jak poniżej

.. code-block:: python

  idx = 0
  for element in sekwencja:
      if idx > len(sekwencja)/2:
          # zrób coś z elementami
          # z drugiej połowy sekwencji

możemy użyć funkcji ``enumerate(sekwencja)``, która zwraca krotkę par ``(indeks, element)``. Aby użyć obu wielkości jednocześnie musimy napisać

.. activecode:: pl_main_0806
    :caption: przykład enumerate

    sekwencja = sorted([123, 12, 12, 1, 4, 1, 124, 13, 441])
    suma = 0
    for indeks, element in enumerate(sekwencja):
        if indeks > len(sekwencja)/2:
            suma += element
    print('Suma elementów większych od mediany:', suma)

.. topic:: Ćwiczenie

    ABC
