.. _typy_zlozone:

*****************************************
Kilka najpopularniejszych typów złożonych
*****************************************

Czasem wygodnie jest grupować jakieś obiekty, np. tworząc listę nazwisk uczniów w klasie, listę ulubionych utworów Pearl Jam, albo też tworząc katalog książek. Możemy też w odobnych kolekcjach trzymać oceny z konkretnych przedmiotów. Budowanie osobnych zmiennych dla np: 5 ulubionych piosenek Pearl Jam jest wykonalne, ale niezbyt wydajne

.. code-block:: python

  pj_miejsce_1 = 'Black'
  pj_miejsce_2 = 'Crazy Mary'
  pj_miejsce_3 = 'Rearviewmirror'
  pj_miejsce_4 = 'Better Man'
  pj_miejsce_5 = 'Sirens'

Znacznie rozsądniej było by stworzyć listę tych utworów i na pierwszym miejscu *listy* dać ulubiony utwór, na drugim - drugi ulubiony itd...

.. index:: list, lista, obiekt modyfikowalny

Listy
~~~~~

Z pomocą może przyjść nam **lista** (pythonowski typ **list**).
Lista jest najbardziej ogólnym rodzajem sekwencji. Podobnie jak :ref:`ciąg znaków <znaki>` obiekty tego typu to uporządkowane sekwencje. Podstawową zaletą list jest prostota przetwarzania ich elementów. Operacje na nich są podobne do tych, które możemy wykonywać na łańcuchach (możemy je wycinać, odwoływać się po indeksach, sprawdzać ich długość). Zmienną o wybranej nazwie i typie ``list`` możemy stworzyć na kilka sposobów. Najbardziej podstawowym będzie skorzystanie z nawiasów kwadratowych, który jest **interfejsem** dedykowanym właśnie dla list

.. code-block:: python

    L1 = [10, 20, 30]
    L2 = []

Pierwszy obiekt o nazwie ``L1`` będzie listą o 3 elementach liczbowych ``10, 20, 30``, a drugi ``L2`` będzie pustą listą (listą nieposiadającą żadnych elementów). W związku z tym długość tej pierwszej to właśnie 3 a drugiej 0 - możecie to sprawdzić tą samą funkcją, którą sprawdzaliście długość ciągu znaków ``len(lista)``. Wracając do przykładu z listą utworów, wyglądało by to tak

.. activecode:: pl_main_0501
    :caption: Pierwsza piątka Pearl Jam

    pj = ['Black', 'Crazy Mary', 'Rearviewmirror', 'Better Man', 'Sirens']
    print(pj)


Jak widać na pierwszym miejscu listy jest piosenka, która poprzednio była przypisana do zmiennej oznaczającej pierwsze miejsce. Aby odwołać się do tej nazwy, wykorzystamy identyczne odwołanie poprzez indeks jak w przypadku łańcuchów znaków

.. code-block:: python

    pj[0]

zwróci ``'Black'``.

.. index:: łączenie list, konkatenacja list

Podobnie jak łańcuchy znaków, listy można łączyć za pomocą operatora ``+``.
Pisząc ``L1 + L2`` tworzymy po prostu now listę powstałą
ze sklejenia obu tych list. Nowa lista będzie składała się z wszystkich elementów obu list, z tym, że na początku znajdą się elementy listy ``L1``, a na końcu listy ``L2``.

.. activecode:: pl_main_0502
    :caption: Pierwsza dziesiątka Pearl Jam

    pj1 = ['Black', 'Crazy Mary', 'Rearviewmirror', 'Better Man', 'Sirens']
    pj2 = ['Alive', 'Yellow Ledbetter', 'Jeremy', 'Even Flow', 'Once']
    pj = pj1 + pj2
    print(pj)


Listy możemy modyfikować. Najprostszą modyfikacją jest zmiana konkretnego elementu za pomocą odwołania ``lista[indeks]`` i użycie operatora przypisania. Możemy, np: zdecydować, że od dzisiaj 5 najlepszym utworem Pearl Jam nie będzie dla nas *Sirens*, ale *Hail, Hail*. W takim wypadku wystarczy, że odwołamy sie do piątego miejsca w liście (pamiętając, że zaczynamy numerację od zera) i przypisanie tam nowego utworu

.. showeval:: showEval_list_mod
   :trace_mode: true

   pj1 = ['Black', 'Crazy Mary', 'Rearviewmirror', 'Better Man', 'Sirens']
   ~~~~
   {{'Sirens' --> 'Hail, Hail'}}{{pj1[4] = 'Hail, Hail'}}
   {{pj1[4] = 'Hail, Hail'}}{{}}
   pj1 --> ['Black', 'Crazy Mary', 'Rearviewmirror', 'Better Man', {{'Sirens'}}{{'Hail, Hail'}}]

Spróbujcie sami, możecie też zmienić *Hail, Hail* na zupełnie inny tytuł.

.. activecode:: pl_main_0503
    :caption: modyfikacja poprzez odwołanie do indeksu

    pj1 = ['Black', 'Crazy Mary', 'Rearviewmirror', 'Better Man', 'Sirens']
    print('Oryginalna lista', pj1, sep='\n')
    pj1[4] = 'Hail, Hail'
    print('Zmodyfikowana lista', pj1, sep='\n')


Podobnie jak w przypadku łańcuchów, możemy stosować :ref:`wycinanie sekwencji <wycinanie_sekwencji>`, poprzez znane już odwołanie

.. code-block:: python

  lista[indeks_startowy:indeks_końcowy:krok]


Modyfikacje i inne operacje specyficzne dla list
------------------------------------------------

.. index:: odwołanie przez kropkę, PZO, obiekt, metoda, pole

Język Python jest językiem obiektowym. Nieco dokładniej opowiemy o tym na końcu tego podręcznika, na tą chwilę wystarczy wiedzieć, że obiekt rozumiany jest jako taki twór, który zawiera w sobie zarówno jakieś dane jak i metody manipulacji tymi danymi. Dane możecie rozumieć jako po prostu pewne wartości, które przypisujecie do zmiennych. Przykładowo ``L = [1, 2, 3]`` będzie listą o nazwie ``L``, która będzie miała trzy elementy ``1, 2, 3``.

Aby wykonać jakąś operację na takiej liście (jak i innych obiektach) wykorzystując metody, które zaszyte są w nich samych posługujemy się tak zwanym **odwołaniem przez kropkę**. Składnia takiego odwołania to

.. code-block:: python

    # odwolanie do metody
    obiekt.metoda(argumenty)
    # odwolanie do zmiennej (pola)
    obiekt.zmienna

.. index:: append

Listy mają takich metod kilka, omówimy je na końcu tej części po kolei. Na razie skupimy się na jednej z najczęściej wykorzystywanych - metodzie ``append`` służącej do rozszerzania listy o kolejny element. Jeżeli mamy rzeczoną już listę pięciu ulubionych utworów Pearl Jam ``pj1``, ale chcemy do niej dodać dwa kolejne utwory, to możemy napisać

.. activecode:: pl_main_0504
    :caption: modyfikacja poprzez odwołanie do indeksu

    pj1 = ['Black', 'Crazy Mary', 'Rearviewmirror', 'Better Man', 'Sirens']
    print('Oryginalna lista', pj1, sep='\n')
    pj1.append('Hail, Hail')
    print('Rozszerzona lista', pj1, sep='\n')
    pj1.append('You Are')
    print('Lista 7 elementowa', pj1, sep='\n')

Mamy nadzieję, że jest to zrozumiałe, bo teraz czeka was ćwiczenie...

.. topic:: Ćwiczenie

    Korzystając z metody ``append`` rozszerz oryginalną listę ``pj1 = ['Black', 'Crazy Mary', 'Rearviewmirror', 'Better Man', 'Sirens']`` o kolejne 5 utworów: 'Alive', 'Yellow Ledbetter', 'Jeremy', 'Even Flow', 'Once' (lub jakichkolwiek innych). Możesz wykorzystać powyższy *ActiveCode* lub zaprogramować odpowiedź w notatniku Jupyter.


Zmienne będące listami mają niezwykle rozbudowaną możliwość manipulacji samymi sobą. Mamy do dyspozycji następujące metody

* ``L.append(obj)``: dodaje element na koniec listy ``L``
* ``L.extend(innaL)``: rozszerza listę poprzez dołączenie wszystkich
  elementów podanej listy ``nowaL``
* ``L.insert(idx, obj)``: wstawia element ``obj`` na podaną pozycję
  listy ``idx``
* ``L.remove(obj)``: usuwa pierwszy napotkany element ``obj`` z listy;
  jeżeli nie ma na liście takiego elementu, zgłaszany jest błąd
* ``L.pop([idx])``: usuwa element z podanej pozycji ``idx`` na liście i
  zwraca go jako wynik; jeżeli nie podano żadnego indeksu a.pop(),
  usuwa i zwraca ostatni element z listy
* ``L.index(obj)``: zwraca indeks pierwszego elementu ``obj`` listy
* ``L.count(obj)``: zwraca liczbę wystąpień elementu ``obj`` w liście
* ``L.sort()``: sortuje elementy listy w niej samej; jest to operacja
  nieodwracalnie modyfikująca listę ``L``
* ``L.reverse()``: odwraca kolejność elementów listy w niej samej

Wypróbujemy kilka z nich: Stworzymy listę L, dodamy do niej kwadraty liczb od
1 do 10. Następnie usuniemy i dodamy różnymi metodami kilka liczb.

.. activecode:: pl_main_0505
    :caption: kilka operacji na listach

    L = [10, 20, 30]
    print('start: L =', L)

    L.insert(0, 1001)
    L.append(100)
    print('append + insert: L =', L)

    L.pop(2)
    del L[2]
    print('pop + del: L =', L)

    L.reverse()
    print('reverse: L =', L)


.. index:: tuple, krotka

Krotki
~~~~~~

Krotki (``tuple``) to takie listy, których nie możemy modyfikować. Tworzymy je
za pomocą nawiasów okrągłych ``()``.

.. code-block:: python

    krotka_trzyelementowa = (1, 2, 3)
    krotka_jednoelementowa = ('Ala',)
    krotka_pusta = ()

W powyższym przykładzie ``krotka_trzyelementowa`` to krotka 3 elementowa
(``len(krotka_trzyelementowa)`` zwróci liczbę 3),
``krotka_jednoelementowa`` to krotka jednoelementowa
a ``krotka_pusta`` to krotka pusta.
Zwróćcie uwagę na obowiązkowy przecinek podczas tworzenia jednoelementowych krotek ``('Ala',)``. Krotki możemy łączyć za pomocą operatora ``+``

.. activecode:: pl_main_0506
    :caption: konkatenacja krotek

    k1 = (1, 2, 3)
    k2 = ('Ala', 'ma', 'kota')
    k3 = k1 + k2
    print(k3)

Krotki jako obiekty niemodyfikowalne mają bardzo ograniczone metody. Do
dyspozycji mamy tylko metody ``count`` oraz ``index``, działające tak
jak odpowiednie metody list.

.. activecode:: pl_main_0507
    :caption: metody krotek

    k1 = (1, 1, 3, 2, 3, 3)
    print('k1.count(3):', k1.count(3))
    print('k1.index(3):', k1.index(3))

Elementu krotki nie można też usunąć poleceniem ``del`` właśnie dlatego, że nie możemy ich modyfikować.


.. index:: dict, słowniki

Słowniki
~~~~~~~~

Oprócz dwóch powyższych **typów sekwencyjnych**, kolejnym niezwykle użytecznym i bardzo często wykorzystywanym typem złożonym są **słowniki**. Od list, krotek i łańcuchów odróżnia je brak uporządkowania elementów oraz fakt, że każdy element słownika składa się z dwóch części - klucza i wartości. Klucz jest wielkością identyfikującą wartość w słowniku. Słowniki budujemy z wykorzystaniem nawiasów klamrowych

.. code-block:: python

  slownik = {klucz1: wartosc1, klucz2: wartosc2, klucz3: wartosc3}


Jeżeli chcielibyśmy na przykład stworzyć bazę informacji o urodzinach naszych znajomych, możemy wykorzystać właśnie słownik. Kluczami oznaczymy imiona znajomych, a wartością będzie data urodzin


.. code-block:: python

    urodziny = {'Ania': '12 stycznia', 'Bartek': '17 lutego', 'Celina': '2 sierpień'}


Gdy chcemy przypomnieć sobie datę urodzenia Ani, musimy odwołać się do elementu którego ``klucz='Ania'``, za pomocą nawiasów kwadratowych, podobnie jak to było w przypadku list, krotek i łańcuchów znaków.


.. activecode:: pl_main_0508
    :caption: Słownik urodzin

    urodziny = {'Ania': '12 stycznia', 'Bartek': '17 lutego', 'Celina': '2 sierpnia'}
    print(urodziny['Ania'])


W ten sposób możemy dowiedzieć się w którym dniu nasi znajomi mają urodziny, jeżeli przez przypadek wyjdzie nam to z głowy.

.. topic:: Ćwiczenie

    Wypróbujcie powyższy *ActiveCode* podmieniając imię na inne z listy. Możecie też dopisać swoje własne pary ``imię: data_urodzenia``.

W dość prosty sposób możemy zmodyfikować już istniejące elementy słownika. Wystarczy odwołać się do elementu słownika za pomocą klucza i po prostu przypisać nową wartość

.. activecode:: pl_main_0509
    :caption: Modyfikacja elementu słownika

    urodziny = {'Ania': '12 stycznia', 'Bartek': '17 lutego', 'Celina': '2 sierpnia'}
    print(urodziny)
    urodziny['Ania'] = '30 sierpnia'
    print(urodziny)

Podobnie do poprzednio omawianych typów złożonych, słowniki mają wiele wbudowanych metod, które umożliwiają operacje na nich. Wszystkie dostępne są z wykorzystaniem odwołania przez kropkę. Omówimy je po części za chwilę. Teraz skupimy się na możliwości powiększania słownika o kolejne elementy. Najprostsza możliwość wykorzystuje podanie nowego klucza i wartości zypełnie jakbyśmy chcieli zmodyfikować już istniejącą parę

.. activecode:: pl_main_0510
    :caption: Dodanie elementu słownika

    urodziny = {'Ania': '30 sierpnia', 'Bartek': '17 lutego', 'Celina': '2 sierpnia'}
    urodziny['Marcin'] = '20 listopada'
    print(urodziny)

.. topic:: Ćwiczenie

    W powyższym *ActiveCode* w podobny sposób dodajcie imię i dzień urodzenia kogoś kogo znacie. Może też to być ktoś znany, jak Albert Einstein.

.. index:: del

Aby w prosty sposób usunąć parę ``klucz: wartość`` ze słownika, możemy użyć polecenia ``del``, które to służy do usuwania zmiennych z przestrzeni nazw. Wystarczy napisać

.. activecode:: pl_main_0511
    :caption: Usuwanie elementu słownika

    urodziny = {'Ania': '30 sierpnia', 'Bartek': '17 lutego', 'Celina': '2 sierpnia'}
    del urodziny['Celina']
    print(urodziny)

Oprócz tych podstawowych operacji wykorzystujących interfejs nawiasów kwadratowych, słowniki możemy modyfikować za pomocą metod wbudowanych w nie same. Poniżej znajdziecie listę metod wraz z krótkim ich omówieniem. Zakładamy, ze ``s`` jest słownikiem


* ``s.clear()`` Usuwa wszystkie elementy ze słownika ``s``
* ``s.copy()`` Zwraca kopię słownika ``s``
* ``s.fromkeys(klucze, domyslna_wartosc)`` Zwraca słownik ``s`` z określonymi kluczami ``klucze`` i wartością ustawioną na opcjonalną zmienną ``domyslna_wartosc``
* ``s.get(klucz, domyslna_wartosc)`` Zwraca wartość podanego klucza ``klucz``, jeżeli klucza nie ma zwraca wartość zmiennej ``domyslna_wartosc``
* ``s.items()`` Zwraca listę zawierającą krotkę dla każdej pary ``klucz: wartość``
* ``s.keys()`` Zwraca listę zawierającą klucze słownika
* ``s.pop(klucz)`` Usuwa element z określonym kluczem ``klucz``
* ``s.popitem()`` Usuwa ostatnio wstawioną parę klucz-wartość
* ``s.setdefault(klucz, wartosc)`` Zwraca wartość określonego klucza. Jeśli klucz nie istnieje tworzy w słowniku ``s`` parę ``klucz: wartosc``
* ``s.update(d)`` Aktualizuje słownik ``s`` o elementy słownika ``d`` nadpisując istniejące pary
* ``s.values()`` Zwraca listę wszystkich wartości w słowniku

Poznane operacje pozwalają na dość swobodną manipulację słownikami.
Możemy na przykład użyć dwóch sposobów by zmodyfikować słownik dat urodzin.

.. activecode:: pl_main_0512
    :caption: Modyfikacja elementu słownika

    urodziny = {'Ania': '30 sierpnia', 'Bartek': '17 lutego', 'Celina': '2 sierpnia'}
    print(urodziny)
    # 1
    urodziny['Celina'] = '3 sierpnia'
    print(urodziny)
    # 2
    urodziny.update({'Celina': '4 sierpnia'})
    print(urodziny)
