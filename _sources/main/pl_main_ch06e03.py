# Spróbuj przepisać tak powyższy program, by pytał użytkownika o nazwę
# miesiąca i na jej podstawie zwracał informację o liczbie dni.

dni_31 = [1, 3, 5, 7, 8, 10, 12]
dni_30 = [4, 6, 9, 11]
miesiac = int(input('Podaj numer miesiąca (od 1 do 12): '))
if miesiac == 2:
   print('{} miesiąc ma 28 lub 29 dni'.format(miesiac))
elif miesiac in dni_31:
   print('{} miesiąc ma 31 dni'.format(miesiac))
elif miesiac in dni_30:
   print('{} miesiąc ma 30 dni'.format(miesiac))
else:
   print('Rok ma 12 miesięcy')
