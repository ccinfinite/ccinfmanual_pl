*********
Przedmowa
*********

W 1977 roku Ken Olsen, założyciel i wieloletni dyrektor Digital Equipment
Corporation, powiedział, że nie jest w stanie podać jednego powodu, by ktoś
hciał mieć w domu komputer osobisty. Co ciekawe, sam był posiadaczem takiego
komputera. Jeżeli mówimy o dziedzinie informatyki były to oczywiście czasy
prehistoryczne. Dzisiaj nikt nie zastanawia się nad użytecznością komputerów.
Czasami pomagają leczyć ludzi, czasami służą po prostu do utrzymywania kontaków
ze znajomymi. Komputer osobisty można dziś znaleźć w ponad 48% gospodarstw
domowych na całym świecie.
Jest dziś używany średnio przez 15% ludzi na świecie.
Do tego lekko ponad 35% ludzkości używa smartfonów, które też przecież są
komputerami.
Jeżeli dodamy do tego, że średnio korzystamy z Internetu przez ponad 6 godzin
dziennie, okaże się, że przepowiednia Pana Olsena była jedną z najbardziej
chybionych opini w historii.
W sumie to przebywamy w dwóch nierzeczywistych krainach przez ponad połowę
swojego życia jako, że śpimy średnio blisko 7 godzin dziennie.

Większość z nas korzysta z komputera, smartfonu, ewentualnie tabletu za pomocą
aplikacji.
Wszyscy używamy przeglądarki do przeglądania internetu czy dedykowanych
aplikacji do oglądania czy edytowania zdjęć.
Używamy programów do pisania listów, wypracowań, maili, kontaktowania się przez
media społecznościowe. Część z nas pracuje używając bardziej wyspecjalizowanych
aplikacji, innym wystarczy rzeczona przeglądarka internetowa i klient poczty.

Czasami spotykamy się z sytuacją, w której program z jakiego właśnie korzystamy,
umożliwia wykonanie wszystkich niezbędnych zadań, czasem braknie nam pewnej
funkcjonalności i musimy radzić sobie inaczej. Czasem uda się znaleźć program
zastępczy, który rozwiąże nasze problemy, czasem nie. Czasami wykonanie
zaplanowanych zadań będzie niezwykle czasochłonne, ale możliwe do wykonania
przy użyciu znanych nam programów komputerowych. Wtedy potrzebujemy tylko
czasu i cierpliwości (i najlepiej kogoś, kto po nas sprawdzi, czy wszystko jest
w porządku).

Sednem nauki programowania nie jest opanowanie jednego z języków programowania.
Tych jest tysiące - od tych najbardziej
`rozpowszechnionych <https://www.tiobe.com/tiobe-index>`_ jak Java, C, Python,
C++, C#, PHP, po te niezwykle ezoteryczne jak historyczny już INTERCAL czy
minimalistyczny Brainf*ck.
Jest to oczywiście krok niezbędny, ale nie jest głównym celem.

Najważniejsza umiejętość, którą się nabywa to zdolność do zrozumienia sedna
problemu przed którym stoimy oraz poprawne rozłożenie go na części pierwsze,
takie jakie będziemy w stanie łatwo rozwiązać a następnie złożenie tych
rozwiązań w całość tak, by dostać odpowiedź na oryginalnie postawione pytanie.
Tak przebiega właśnie typowa praca programisty. Metoda ta ma nawet swoją nazwę
- dziel i zwyciężaj i jest jednym z głównych paradygmatów programowania i
algorytmiki.

To właśnie nauka tego szablonu może być największą wartością dodaną tego kursu.
Możena próbować znajdować wyjście z różnych sytuacji życiowych właśnie poprzez
rozwiązywanie mniejszych części dużego problemu przed którym stoimy. Czasami
warto też zaprogramować sobie dzień krok po kroku.

.. note::
   Poniższe programy można wypróbować od razu w notatniku Jupyter (IPython).
   Proszę ściągnąć ten plik, wgrać go na wasz serwer Jupyter lub otworzyć
   w Google Colaboratory.

   plik: `notatnik <https://nbviewer.org/urls/bitbucket.org/ccinfinite/ccinfmanual_pl/raw/89b4f27ba83fb8023821a8133305d5bb328b4b3f/_sources/main/pl_main_01_intro_code.ipynb>`_.

Katalogowanie zdjęć
===================

Pora zapewne na jakiś przykład, jakieś konkretne zastosowanie tego całego
programowania. Wiele osób posiada w komputerach lub na zewnętrznych twardych
dyskach cyfrowym archiwum zdjęć. Często zdjęcia te trzymamy w katalogach,
katalogi te nazywamy miejscem lub wydarzeniem, czasami do nazwy dodajemy datę,
a czasami mamy katalogi nadrzędne powiązane z datą, np:

::

    -2019
      ᒻ--Wycieczka do Krakowa
      ᒻ--Pokaz sztucznych ogni
      ᒻ--Impreza u Tadka

    -2018
      ᒻ--Wakacje w Gdyni
      ᒻ--Weekend w Zakopanem

Zdjęcia jakie znajdują się w środku, najcześciej mają nazwę nadaną przez aparat
fotograficzy, co z reguły wygląda tak

::

  IMG20180821232.JPG  IMG20180821233.JPG  IMG20180821234.JPG  IMG20180821235.JPG
  IMG20180821236.JPG  IMG20180821237.JPG  IMG20180821238.JPG  IMG20180821239.JPG
  IMG20180821240.JPG  IMG20180821241.JPG  IMG20180821242.JPG  IMG20180821243.JPG
  IMG20180821244.JPG  IMG20180821245.JPG  IMG20180821246.JPG  IMG20180821247.JPG

Może, tak po prostu z zamiłowania do porządku, chcielibyśmy, aby zdjęcia z
katalogów posiadały również znaczące nazwy, od razu identyfikujące zawartość
zdjęcia, na przykład `2018_Wakacje_w_Gdyni_001.jpg`. Najprostszą i najbardziej
uciążliwą metodą będzie po prostu ręczna zmiana nazw tych wszystkich plików.
Inną może być wykorzystanie jednego z programów graficznych, które taką zmianę
potrafią przeprowadzić w tzw. trybie wsadowym (w serii).
Można też napisać prosty program w języku Python...

.. literalinclude:: pl_main_ch01e01.py
   :linenos:
   :caption: Link do :download:`tego skryptu <pl_main_ch01e01.py>`.


Jedyne co należy wiedzieć, to że w miejscu `"2019"` musimy wpisać nazwę
katalogu, czy też rczej roku który nas interesuje, a w miejscu
`"Impreza u Tadka"` wpisujemy nazwę katalogu gdzie przechowywujemy zdjęcia
których nazwy właśnie chcemy zmienić. Oczywiście - musimy też wiedzieć jak
taki program uruchomić, ale nie musimy kompletnie przejmować się samą zmianą
nazw, czy tym że popełnimy błąd przy numerowaniu zdjęć. Jak już raz opanujemy
umiejętność uruchamiania programów, które sami napisaliśmy np. w języku Python,
to za każdym razem będziemy podawać te same polecenia.

Wygląda to nieco skomplikowanie, ale do wszystkiego dojdziemy po kolei. Co
ważne to to, że wystarczy nam 12 lini kodu (choć tak naprawdę jest ich 10),
by komputer sam zrobił za nas całą pracę. Co więcej - ten program dość prosto
można zmienić tak, by sam przeszukiwał główny folder w którym trzymamy zdjęcia
i automatycznie zmieniał nazwy wszytskich plików w podobny sposób - dając na
początku nazwy zdjęcia rok, potem nazwę imprezy a na końcu własną numerację.


OK, czas na nieco bardziej akademicki problem...

Jak często jakieś słowo występuje w tekście pisanym?
====================================================

Na pewno każdy kiedyś czytając książkę zastanawiał się nad tym, które słowo
najczęściej w niej występuje. Albo, czy istnieje jakaś reguła opisująca związek
z częstotliwością występowania słowa a pozycją w rankingu słów najczęściej
spotykanych w książce... Ok, prawdopodobnie nikt się nad takimi rzeczami nie
zastanawia, raczej wszyscy skupiają się na treści książki ;)
Niemniej jednak jest to całkiem ciekawy problem (przynajmniej w naszych oczach).

Cała idea polega na tym, by policzyć ile razy dany wyraz występuje w
rozpatrywanym tekście. Jeżeli weźmiemy pod uwagę prosty tekst, np. piosenkę "Hello, Goodbye" zespołu The Beatles, to możemy po prostu policzyć ile razy dane słowo w nim występuje.

    You say, „Yes”, I say, „No”

    You say, „Stop” but I say, „Go, go, go”

    Oh no

    You say, „Goodbye”, and I say, „Hello, hello, hello”

    I don’t know why you say, „Goodbye”, I say, „Hello, hello, hello”

    I don’t know why you say, „Goodbye”, I say, „Hello”

Nie bierzmy pod uwagę wielkości liter, bo jednak wciąż te same wyrazy. Polecamy każdemu policzyć samodzielnie ile razy dane słowo, a jest ich 15,  występuje w tej piosence, a potem ułożyć słowa od najczęściej do najrzadziej występujących. Powinniście dostać taką tabelkę

========== ==========
say        10
i          7
hello      7
you        5
go         3
goodbye    3
no         2
don’t      2
know       2
why        2
yes        1
stop       1
but        1
oh         1
and        1
========== ==========

Zgadza się wam? Jeżeli tekst jest prosty, to i zadanie nietrudne. Spróbujmy z nieco bardzej skomplikowanym tekstem.

    Przeprowadził swój plan bardzo zręcznie. Zwyczajny łotr byłby użył psa rozjuszonego — on nadał mu jeszcze pozory piekielnej bestii. Kupił najdziksze i największe psisko, jakie mógł dostać u handlarzy Ross and Mangles w Londynie. Przywiódł go do stacji North Devon, odległej od Baskerville-Hall i nadłożył ogromny kawał drogi, idąc łąką i moczarami, aby go nikt nie spostrzegł. Wpierw wynalazł dla niego schronienie w Grimpen-Mire i tam go od razu wprowadził. Trzymał go na łańcuchu i czekał sposobności. Ale niełatwo było ją znaleźć.

    Stary baronet nie wychodził nigdy za obręb pałacu po zachodzie słońca. Kilkakrotnie Stapleton czyhał na niego z psem, lecz bezskutecznie. Wtedy to okoliczni włościanie widzieli ogromne psisko, co spowodowało wskrzeszenie legendy.

Teraz już nie tak łatwo. Po pierwsze - mamy do czynienia z dużo większą ilością wyrazów. Po drugie najczęściej się one wcale nie powtarzają. Nie będziemy tu prezentować całej tabeli, bo to aż 99 wierszy, ale pierwsze 12 wygląda tak

================ ================
i                5
go               4
psisko           2
w                2
od               2
nie              2
niego            2
na               2
przeprowadził    1
swój             1
plan             1
bardzo           1
================ ================

Komputer jest idealnym narzędziem do wykonywania tych samych zadań w kółko.
Informatycy mówią, że aby wykonać tą samą czynność wiele razy, trzeba nakazać komputerowi użyć pętli.
Przykład jej wykorzystania widzieliście już w poprzednim zagadnieniu.

W poniższym programie pomysł użycia pętli występuje trzy razy. W pierwszym sprawdzamy literka po literce, czy znak oryginalnego tekstu występuje w angielskim alfabecie. Za drugim razem zliczamy wyrazy, jeden po drugim. Trzecia pętla `for` służy do pokazania wyników - od najczęściej do najrzadziej występującego wyrazu.

Sam program jest interaktywny, możecie go uruchomić klikając przycisk `Run`, możecie też go zmieniać, psuć i naprawiać.

.. activecode:: pl_main_0101
   :coach:
   :caption: Interaktywny program zliczający ile razy dane słowo występuje w tekście.

   txt = '''
   You say, „Yes”, I say, „No”
   You say, „Stop” but I say, „Go, go, go”
   Oh no
   You say, „Goodbye”, and I say, „Hello, hello, hello”
   I don’t know why you say, „Goodbye”, I say, „Hello, hello, hello”
   I don’t know why you say, „Goodbye”, I say, „Hello”
   '''

   fixed_txt = ''
   for letter in txt.replace('\n', ' '):
       l = letter.lower()
       if l in 'abcdefghijklmnopqrstuvwxyz ’':
           fixed_txt += l

   zipf = {}
   for word in fixed_txt.split():
       if word in zipf:
           zipf[word] += 1
       else:
           zipf[word] = 1

   for z in sorted(zipf.items(), key=lambda x: x[1], reverse=True):
       print('{0[0]}\t{0[1]}'.format(z))

Zerknijcie w linie 11, 17 i 23. Są tam słowa kluczowe ``for``. Oznacza ono, że właśnie zaprogramowaliśmy użycie pętli. Jest to mechanizm, który umożliwia wykonywanie jakiś poleceń w kółko.


.. topic:: Ćwiczenie

   Podmieńcie tekst ``txt = '''...'''`` na inny, dowolny, np. tekst waszej ulubionej piosenki. Następnie uruchomcie program jeszcze raz. Jak zmieniła się tabelka?



No tak, ale jak wygląda to dla całej książki? Powyższy fragment prozy pochodzi z powieści detektywistycznej sir Arthura Conana Doyle’a zatytuowanej Pies Baskerville’ów. W tłumaczeniu, które znajdziecie w serwisie `wolnelektury.pl <https://wolnelektury.pl/katalog/lektura/pies-baskervilleow.html>`_ cały tekst liczy ponad 34 tysiące słów i stawia przed nami raczej niewykonalne zadanie. To znaczy - można oczywiście policzyć dane słowa, posługując się jakąś systematyczną metodą, ale nieukrywajmy - tego typu sekwencyjne zadania "znajdź i dodaj" powinna wykonywać raczej maszyna.

To jak sobie poradzić z całą książką?

.. literalinclude:: pl_main_ch01e02.py
   :linenos:
   :emphasize-lines: 4,10,20
   :caption: Link do :download:`tego skryptu <pl_main_ch01e02.py>`.

Skopiujcie ten kod do notatnika Jupyter, lub też sciągnijcie i uruchomcie program w konsoli. Powinien Wam obliczyć liczebności i pokazać pierwszych 10 najczęściej występujących słów w książce.



A teraz sprawdźcie co zapamiętaliście...

.. mchoice:: question1_intro
    :multiple_answers:
    :correct: b,c
    :answer_a: Wszystkie słowa są jednakowo prawdopodobne.
    :answer_b: Słowo na pozycji (n) pojawia się (1/n) mniej razy niż słowo, które pojawia się najczęściej.
    :answer_c: Prawdopodobieństwo obserwacji jest odwrotnie proporcjonalne do jej rangi.
    :answer_d: Powinniśmy przeczytać więcej książek.
    :feedback_a: Niezupełnie dobrze. Pomyśl, jak popularne jest słowo "the", a jak niepopularne jest słowo "Brexit".
    :feedback_b: Prawda. Jest to najczęstsze stwierdzenie dotyczące prawa Zipfa.
    :feedback_c: Prawidłowo. To byłoby najbardziej ogólne znaczenie prawa Zipfa.
    :feedback_d: Zdecydowanie powinniśmy czytać, dużo czytać, ale Zipf nigdy tego nie stwierdził.

    Co oznacza prawo Zipfa?

.. fillintheblank:: intro_fill_01

    Jaka komenda języka Python oznacza pętlę?

    - :for|while: Dokładnie tak!
      :.*: Niekoniecznie... Zerknijcie w powyższy tekst.


.. index:: komentarz

Komentarz
=========

Komentarze w języku Python poprzedzone są znakiem ``#``.
Wszystkie znaki następujące po ``#``, a ciągnące się do końca linii, są
ignorowane [#komentarz]_.

.. code-block:: python

    >>> # pierwsza linia komentarza
    >>> print("Witaj świecie")  # druga linia komentarza

Za pewnego rodzaju komentarz można uznać dokumentację funkcji czy klas tworzoną przez tekst wpisany w potrójne cudzysłowy lub apostrofy. Nie jest to komentarz *per se*, ale może za taki uchodzić. Wiecej o dokumentacji przeczytacie w dalszej części tego manuala.

.. code-block:: python

    """
    ten tekst, poprzedzony jest,
    oraz zakończony trzema cudzysłowami,
    jest więc zwykłym literałem znakowym,
    który często wykorzystywany jest do
    tworzenia dokumentacji programów
    """


Język programowania
===================

Program komputerowy stworzony z użyciem jakiegoś języka programowania jest najczęstszą metodą na zmuszenie komputera by wykonał zaplanowaną przez nas pracę. Jeżeli ta praca nie polega na odpisywaniu na emaile, ale raczej na wykonaniu jakiś cyklicznych czynności, które są mniej standardowe, to napisanie własnego programu będzie prawdopodobnie też metodą najprostszą. Brzmi to zapewne dziwinie na samym początku nauki programowania, ale mamy nadzieję, że pod koniec tego kursu zmienicie zdanie. Język programowania można rozumieć jako sposób komunikacji z komputerem, który jest zrozumiały dla człowieka. Na pewno można porównać go do języka naturalnego, który umożliwia komunikację między ludźmi. Jest jednak jedna różnica. W przypadku języków programowania nie ma miejsca na niedopowiedzenia czy interpretację. Wszytskie komunikaty muszą być zapisane bardzo precyzyjnie. Komputer jest maszyną i nie potrafi (jeszcze) domyślać się o co chodzi w niejednoznacznej wypowiedzi.


.. rubric:: Przypisy

.. [#komentarz] czasem jednak mogą prowadzić do kłopotów związanych z problemmi ze standardem kodowania tekstu użytego w skrypcie a tym zadeklarowanym w nagłówku skryptu. Najbezpieczniej używać systemu UTF-8.
