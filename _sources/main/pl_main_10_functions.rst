.. _pojecie_funkcji:

***************
Pojęcie funkcji
***************

Ta część podręcznika będzie nieco bardziej formalnym wprowadzeniem do funkcji. Podamy pełną definicję oraz opowiemy o parametrach formalnych, argumentach przypisywanych poprzez nazwę, a także o instrukcji return. Możecie potraktować ją jako rozwinięcie tego co opisaliśmy w poprzednim rozdziale i zaglądać tutaj, gdy tylko coś związanego z funkcjami będzie niezrozumiałe.

.. index:: funkcja

Jak mogliście przeczytać wczesniej, poprzez funkcję, będziemy rozumieli nazwany
fragment kodu, do którego możemy się odwoływać wielokrotnie. Przypomnimy jej definicję w języku Python

.. code-block:: python

  def nazwa_funkcji(<lista_argumentow>):
      CIAŁO_FUNKCJI
      <return obiekt>

Funkcje charakteryzują się istnieniem zwracanej przez nie za pomocą instrukcji ``return`` wartości wynikowej.
Używanie funkcji jest intuicyjne. Można przyjąć zasadę, że tam gdzie zachodzi
konieczność powtórzenia kodu, którego zadaniem jest wykonanie tej samej
procedury, tam należy kod przekształcić do postaci funkcji. Kod całego
programu staje się bardziej czytelny i funkcjonalny. Idea funkcji w
programowaniu, jest analogiczna do funkcji w sensie matematycznym. W
odróżnieniu jednak od funkcji matematycznych, funkcje w programowaniu nie
muszą posiadać argumentu (parametru), by być poprawnie zdefiniowane.
Przykładowo:

.. activecode:: pl_main_1001
   :caption: Funkcja bez parametru.

   def funkcja_hej():
       return "Hej! Jak się masz?"

   hej = funkcja_hej()
   print(hej)

Instrukcja ``return``
=====================

.. index:: instrukcja return

W powyższych przykładach funkcje komunikowały się z nami z pomocą instrukcji ``return``. Instrukcja ta zwraca podany obiekt, oraz powoduje zakończenie wykonywania obliczeń przez funkcję. Jeżeli chcemy zwrócić więcej niż jedną zmienną to stosujemy np. krotkę. Aby wywołać funkcję, należy napisać jej nazwę, a po niej krotkę zawierającą argumenty funkcji. Kolejność, oraz liczba parametrów przekazywanych do funkcji, musi zgadzać się z jej definicją:

.. activecode:: pl_main_1002
   :caption: Funkcja obliczająca wartość kwadratu sumy.

   def kwadrat_sumy(x, y):
       return (x + y) ** 2

   print(kwadrat_sumy(2, 1))

Zauważmy, że instrukcji ``return`` można użyć w kodzie funkcji wielokrotnie, ale po dojściu do dowolnego z nich, funkcja przestanie się wykonywać i nastapi z niej wyjście. Przykładowo

.. activecode:: pl_main_1003
   :caption: Funkcja zwracająca większą z dwóch podanych liczb.

   def maksimum(x, y):
       """Funkcja zwracajaca wieksza z dwoch liczb."""
       if x > y:
           return x
       else:
           return y

   print(maksimum(10, -10))

.. index:: docstring

W zależności, od podanej pary liczb, wykonywanie funkcji jest przerywane albo
po instrukcji ``if``, albo po instrukcji ``else``.  Dobrą praktyką
jest dokumentowanie, w formie łańcucha znaków ``""" ... """`` do czego służy
dana funkcja. Łańcuch taki, występujący zaraz po pierwszej linii definiującej
funkcję nazywamy **docstringiem**.  Ponieważ, w kursie tym, kody źródłowe
omawiane są w sposób szczegółowy to w  kolejnych przykładach nie zawsze
uszanujemy ten zwyczaj. Wy powinniście.

.. topic:: Ćwiczenie

    Napisz funkcję sprawdzającą czy podana liczba jest parzysta czy nieparzysta.

Domyślne wartości parametrów
============================

.. index:: parametry domyślne

Definiując funkcję, możemy przypisać parametrom domyślne wartości. Użyte będą
one wtedy, gdy przy wywoływaniu funkcji, nie zostaną podane inne wartości dla
tych parametrów.

.. topic:: **Trudne zadanie z Fizyki**

    W górę rzucono piłkę. Zaniedbując siły oporu, oblicz na jaką wysokość
    wzniesie się piłka po czasie \\(t\\) sekund, jeżeli wartość początkowa prędkości wynosiła \\(v_0\\) m∕s.
    Wykorzystaj wzór

    .. math::

        h(t) = v_0 t - \frac{1}{2} g t^2

W powyższym wzorze \\(g\\) to przyspieszenie grawitacyjne, które dla Ziemi w przybliżeniu wynosi \\(g=9.80665 m/s^2\\) i przyjmujemy w obliczeniach niewymagających bardzo wysokiej precyzji, że jest stałe (choć nie jest...). Jako, że prędkość początkowa i czas po jakim mierzymy wysokość będzie inna w zależności od eksperymnetu, to powinniśmy je zostawić jako zmienne bez wartości domyślnych, ale dla przyspiesznia grawitacyjnego \\(g\\) możemy już założyć, że z reguły taki rzut odbędzie się na Ziemi. W takim przypadku możemy w definicji funkcji ``wysokosc`` obliczającej wysokość do zmiennej ``g`` podać wartość domyślną dla Ziemi, tak jak poniżej.

.. activecode:: pl_main_1004
   :caption: Funkcja z predefiniowanymi wartosciami parametrow.

   def wysokosc(t, v0, g=9.81):
       """Funkcja wysokosc:
          t - czas
          v0 - predkosci poczatkowa
          g - przyspieszenie grawitacyjne, domyślnie dla Ziemi
        """
       return v0 * t - g * t ** 2 / 2

   print(wysokosc(0.3, 3)) # na Ziemi
   print(wysokosc(t=0.3, v0=3, g=1.622)) # na Ksieżycu

.. topic:: Ćwiczenie

    Napisz funkcję konwertującą odległość podaną w kilometrach, domyślnie na
    mile morskie a opcjonalnie na dowolną inna milę.


Niekreślona ilość parametrów funkcji
==========================================

Język Python dopuszcza by skonstruować funkcję, która nie będzie miała
zdefiniowanej górnej liczby parametrów. Aby to zrobić należy  nazwę listy
parametrów funkcji poprzedzić symbolem ``*``. Funkcję taką możemy wywołać z
dowolną ilością argumentów. Kod zaprezentowany niżej to funkcja wykonująca
sumę tylu elementów ile podane zostanie w krotki ją wywołującej.

.. activecode:: pl_main_1005
   :caption: Funkcja z nieokreśloną liczbą parametrów.

   def dodawanie(*arg):
       suma = 0
       for a in arg:
           suma += a
       return suma

   print(dodawanie(10))
   print(dodawanie(10, 20, 30, 40))

Jak taka konstrukcja będzie działać w połączeniu z innymi parametrami?
Sprawdźmy:

.. activecode:: pl_main_1006
   :caption: Przykład funkcji w której nie jest znana początkowa wartość parametrów.

   def dodawanie(arg1, *arg):
       suma = arg1
       for a in arg:
           suma = suma + a
       return suma

   print(dodawanie(100, 2))
   print(dodawanie(100, 1, 2, 3, 4))

Zagnieżdżanie funkcji
=====================

.. index:: funkcje zagnieżdżone

Definicje funkcji mogą być zawierane w sobie w sposób jawny, tak jak w
przykładzie wyliczającym wartość kosinusa kąta pomiędzy wektorami na
płaszczyźnie:

.. activecode:: pl_main_1007
   :caption: Zawieranie się funkcji w sobie.

   def kosinus(a, b):

       def dlugosc(x):
           return (x[0] ** 2 + x[1] ** 2) ** 0.5

       def iloczyn(x, y):
           return x[0] * y[0] + x[1] * y[1]

       m = dlugosc(a) * dlugosc(b)
       if m > 0:
           return iloczyn(a, b) / m

   print(kosinus([1, 0], [0, 1]))


.. topic:: Ćwiczenie

    Wykorzystując funkcję, napisz program, który dla podanych ``a``, ``b`` oraz
    ``c`` znajduje rozwiązania trójmianu kwadratowego.

    .. math::

        a x^2 + bx + c = 0

    Niech program informuje o ilości tych rozwiązań oraz w zależności od ich
    ilości wypisuje wartości pierwiastków.


.. Rekurencja
.. ==========
..
.. .. index:: rekurencja, dekompozycja, rekursja
..
.. Bardzo ważnym mechanizmem używanym w programowaniu, a ściśle powiązanym z
.. algorytmiką, jest tak zwana **rekurencja** (termin używany często zamiennie z
.. terminem **rekursja**).
..
.. x..note::
..     Rekurencja jest to sposób programowania, w którym funkcja lub definicja,
..     odwołuje  się raz lub więcej razy  do samej siebie.
..
.. Źródłosłów słowa rekurencja wywodzi się z łacińskiego "recurrere", co oznacza
.. "przybiec do siebie". Nasz ludzki  sposób myślenia oparty jest na rekurencji.
.. Wyobraźmy sobie, że mamy przed sobą stos porozrzucanych książek. Dostajemy
.. zadanie by ułożyć książki na półce. Prawdopodobnie większość z nas rozwiąże to
.. zadanie następująco: weźmiemy pierwszą książkę ze stosu, ułożymy ja na półce,
.. a następnie zabierzemy się za pozostałą część stosu. Jak to zrobimy? Jak
.. zrealizujemy drugą część zadania? Zapewne analogicznie do pierwszej.  Weźmiemy
.. drugą  książkę ze stosu i położymy ją na półce. Następnie, wrócimy do stosu,
.. weźmiemy trzecią książkę i ułożymy ją na półce. I tak będziemy postępować do
.. momentu kiedy ułożymy wszystkie książki na półce.  Proszę zwrócić uwagę na dwa
.. fakty, już w tym miejscu stykamy się z pewnym sposobem postępowania. Przepisem
.. na rozwiązanie problemu. Przepis ten będziemy nazywali algorytmem. I o tym
.. będzie traktować dalsza część kursu.  Uwaga druga: zwróćmy uwagę, że złożony
.. problem został rozłożony na problemy elementarne, charakteryzujące się
.. mniejszym stopniem skomplikowania, innymi słowy takie, które łatwiej jest
.. rozwiązać w porównaniu  do problemu wyjściowego.  Rozłożenie problemu na
.. łatwiejsze do rozwiązania składowe będziemy nazywali **dekompozycją**.
..
.. Wróćmy do definicji rekursji. W inny sposób można powiedzieć, że o rekurencji
.. mówimy wtedy gdy definicja pewnego obiektu zawiera odwołanie do transformacji
.. tego samego obiektu. Aby znaleźć  transformację tego obiektu należy ponownie
.. zastosować tę samą definicję. Nie będziemy się zachowywać rekurencyjnie i  tej
.. definicji powtarzać w nieskończoność nie będziemy. Dodajmy jedynie, że  każda
.. definicja rekurencyjna składa się z:
..
.. * problemu bazowego (zwanego też wyrażeniem początkowym startowym lub
..   warunkiem brzegowym)
.. * zależności rekurencyjnej.
..
.. Matematyka obfituje w  przykłady definicji rekurencyjnych, my wymienimy dwie z
.. nich:
..
.. * Silnia:
.. x   .. math::
..
..        n! = \left\{\begin{matrix}
..        1 \quad dla \quad n = 0 \\
..        n(n-1)! \quad dla \quad n \geqslant  1
..        \end{matrix}\right.
..
..    wyrażeniem startowe to: 0!=1, zależność rekurencyjna to: n!=n(n-1)!
..
.. * Współczynnik dwumianowy:
.. x  .. math::
..
..        \binom{n}{k}= \binom{n-1}{k-1}+\binom{n-1}{k}
..
..    (zależność rekurencyjna)
..
.. x    .. math::
..
..        \binom{n}{0}= \binom{n}{n} = 1
..
..    (wyrażenie startowe)
..
.. Skupmy się na znanej zapewne definicji silni. Zaimplementujmy ją w Pythonie:
..
.. .. activecode:: l03_017
..    :caption: Silnia jako przykład funkcji rekurencyjnej.
..
..    def silnia(n):
..        if n == 1:
..            return 1
..        else:
..            return n * silnia(n-1)
..
.. Wywołaj te funkcję, sprawdź, czy działa zgodnie z założeniami.
..
.. Spróbujmy prześledzić  jak ta funkcja działa, zrobimy to poprzez dodanie
.. dwóch funkcji ``print`` w ciele definicji funkcji:
..
.. .. activecode:: l03_018
..    :caption: Silnia jako przykład funkcji rekurencyjnej.
..
..    def silnia(n):
..        print "Funkcja silnia zostala wykonana dla n = " + str(n)
..        if n == 1:
..            return 1
..        else:
..            res = n * silnia(n-1)
..            print "Posredni wynik dla  ", n, " * silnia(" ,n-1, "): ", res
..            return res
..
..    print silnia(5)
..
.. Warto tu nadmienić, że rekurencja jest często alternatywną metodą
.. rozwiązywania problemów do tak zwanej metody iteracyjnej. Podejście iteracyjne
.. polega na n-krotnym wykonaniu algorytmu w taki sposób, aby wyniki uzyskane
.. podczas poprzedniej iteracji (potocznie kroku, lub bardziej fachowo przebiegu)
.. mogły służyć jako dane  wejściowe do kroków kolejnych. Zazwyczaj iteracjami
.. steruje się za pomocą instrukcji pętli. Lepiej, zapewne prześledzić to na
.. przykładzie:
..
.. .. activecode:: l03_019
..    :caption: Silnia obliczana metodą iteracyjną.
..
..    def silnia_iteracyjnie(n):
..        wynik = 1
..        for i in range(2, n+1):
.. 	   wynik *= i
..        return wynik
..    print(silnia_iteracyjnie(5))
..    .. a*
..
.. Rozpatrzmy jeszcze jeden, bardzo popularny przykład funkcji rekurencyjnej.
.. Zdefiniujmy funkcję, ktora będzie liczyła elementy ciągu
.. Fibonacciego:
..
.. x .. math::
..
..    F_n := \left\{\begin{matrix}
..    0 \quad dla \quad n = 0 \\
..    1 \quad dla \quad n = 1 \\
..    F_{n-1} + F_{n-2}\quad dla \quad n  > 1
..    \end{matrix}\right.
..
.. Elementy tego ciągu to liczby naturalne mające taką własność, że kolejny
.. wyraz (nie licząc dwóch pierwszych) jest sumą dwóch poprzednich:
..
.. x .. math::
..
..    \{1,1,2,3,5,8,13, \ldots \}
..
.. Ciąg ten jest specyficzny o tyle, że odzwierciedla wiele zjawisk zachodzących
.. w przyrodzie. Często do zobrazowania tego przyjęło się posługiwać...
.. królikami. Załóżmy, że:
..
.. * zaczynamy z jedną parą królików,
.. * każda para królików wydaje na świat, w miesiąc po kopulacji potomstwo:
..   samicę i samca,
.. * króliki są nieśmiertelne,
.. * w miesiąc po urodzeniu, królik może przystąpić do reprodukcji.
..
.. Pomijając pewne niuanse będące przedmiotem innych rozważań, wzrost populacji
.. takiej króliczej farmy opisany będzie właśnie ciągiem Fibonacciego. Pod koniec
.. pierwszego miesiąca możemy się spodziewać  pierwszego zapłodnienia w pierwszej
.. parze królików. Pod upływie drugiego miesiąca pojawi się  druga para królików.
.. Na farmie będą już dwie pary. W trzecim miesiącu będziemy mieli już trzy pary.
.. Pierwsza samica wyda na świat kolejne potomstwo, a urodzone wcześniej,
.. przystpią do kopulacji. I tak dalej...
..
.. Taki ciąg łatwo jest zaimplementować w Pythonie:
..
.. .. activecode:: l03_020
..    :caption: Ciąg Fibonacciego obliczony rekurencyjnie.
..
..    def fib(n):
..     if n == 0:
..         return 0
..     elif n == 1:
..         return 1
..     else:
..         return fib(n-1) + fib(n-2)
..    print(fib(6))
..
.. Czy podobnie jak w poprzednim przykładzie, potrafiłbyś przekształcić ten kod
.. tak, by za pomocą funkcji ``print`` prześledzić kolejne etapy jego wykonania?
.. Spróbuj!
..
.. Dla porównania zaprezentujmy poniżej metodę iteracyjną wyliczania wartości
.. elementów tego ciągu:
..
.. .. activecode:: l03_021
..    :caption: Ciąg Fibonacciego obliczony iteracyjnie.
..
..    def fib(n):
..        a, b = 0, 1
..        for i in range(n):
..            a, b = b, a + b
..        return a
..
..    print(fib(6))
..
.. .. topic:: Zadanie 3.6
..
..    Napisz funkcję, która zaimplementuje trójkąt Pascala:
..
.. x    .. math::
..
..        \begin{array}{ccccccccccc}
..        &    &    &    &    &  1 &    &    &    &    &   \cr
..        &    &    &    &  1 &    &  1 &    &    &    &   \cr
..        &    &    &  1 &    &  2 &    &  1 &    &    &   \cr
..        &    &  1 &    &  3 &    &  3 &    &  1 &    &   \cr
..        &  1 &    &  4 &    &  6 &    &  4 &    &  1 &   \cr
..        1 &    &  5 &    & 10 &    & 10 &    &  5 &    & 1 \cr
..        \dots \cr
..        \end{array}
