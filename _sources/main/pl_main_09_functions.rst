.. _podprogramy:


***********
Podprogramy
***********

Od czasu do czasu, aby podjąć jakąś decyzję decydujemy się na to, by ślepy los zdecydował za nas. W takich chwilach bardzo często `rzucamy monetą <https://pl.wikipedia.org/wiki/Rzut_monet%C4%85>`_ i w zależności od tego, czy wypadnie orzeł czy reszka wybieramy takie a nie inne rozwiązanie. Przykładowo mecze piłki nożnej czy koszykówki zaczynają się od rzutu monetą. Metoda ta zapewnia, że wybór wygranej strony jest całkowicie przypadkowy i nie zależy od żadnych wcześniejszych rzutów. Czasem takim rzutem rozstrzygamy spór i liczymy na to, że moneta wskaże, że mamy rację. Może uda się nam raz, może trzy razy na kilka takich przypadków - każdy z rzutów jest niezależny! Ale zawsze możemy obliczyć jaka jest szansa na to, że (a) zawsze albo (b) w połwie przypadków będziemy mieli rację. Aby to zrobić posługujemy się rozkładem dwumianowym, który powie nam jakie jest prawdopodobieństwo :math:`k` sukcesów w \\(n\\) próbach

.. math::

    \frac{n!}{k!(n-k)!} p^k (1-p)^{n-k}

Wartość :math:`p=1/2` oznacza, że mamy równą szansę na wyrzucenie orła lub reszki, :math:`p^k` to potęgowanie, a :math:`n! = 1 \cdot 2 \cdot \dots n` to silnia z liczby \\(n\\). Potęgowanie ma swój własny operator w języku Python, więc to działanie możemy zapisać ``p ** k``. Silnia wystepuje jako funkcja w bibliotece ``math``, ale łatwo ją obliczyć korzystając z podanego wzoru i jakiejś pętli. Poniższy kod obliczy silnię z liczby \\(10! = 3628800\\).

.. activecode:: pl_main_0901
    :caption: silnia

    n = 10
    silnia = 1
    for liczba in range(1, n + 1):
        silnia = silnia * liczba
    print(silnia)


To jak już wiemy jak obliczyć silnię i potęgę to możemy sobie obliczyć jaka jest szansa na to, że w 5 przypadkach na 10 wygramy zakład. Wracamy do wzoru, ale podstawimy sobie już konkretne liczby

.. math::

    \frac{10!}{5!(10 - 5)!} 0.5^5 (1-0.5)^{10-5}

Musimy obliczyć więc silnię z liczb 10 i 5, oraz podnieść pół do potęgi piątej. To do dzieła

.. activecode:: pl_main_0902
    :caption: 5 wygranych rzutów monetą na 10 prób
    :enabledownload:


    n = 10
    silnia10 = 1
    for liczba in range(1, n + 1):
        silnia10 = silnia10 * liczba

    n = 5
    silnia5 = 1
    for liczba in range(1, n + 1):
        silnia5 = silnia5 * liczba

    p5 = (silnia10 / (silnia5 * silnia5)) * (0.5 ** 5) * ((1 - 0.5) ** (10 - 5))
    print('Szansa na pięciokrotną wygraną w 10 rzutach to {:.1f}%'.format(p5 * 100))

Spodziewaliście się 50%? Łatwo wpaść w tą pułapkę... Ale to nie zajęcia z kombinatoryki, ale nauka programowania. Wróćmy zatem na chwilę do powyższego kodu. Linie obliczające silnie z liczby 10 (1-4) oraz z liczby 5 (6-9) są w zasadzie kopią tego samego kodu, jedyne co je różni to wartość zmiennej ``n`` i nazwy zmiennych wynikowych (``silnia5`` i ``silnia10``). Zapewne najłatwiej było by obliczyć prawdopobieństwo 5 wygranych, gdybyśmy mieli do dyspozycji operator ``!``, który oblicza silnię z danej liczby naturalnej. Co prawda nie zmienimy biblioteki standardowej języka Python, ale w pewnym sensie możemy taką czynność (wyliczanie silni) zaprogramować budując **podprogram**. Podprogramy mozemy podzielić na procedury oraz **funkcje**. W języku Python mamy do czynienia tylko z tymi ostatnimi.

.. index:: funkcja

Funkcja
===============

Poprzez funkcję, będziemy rozumieli nazwany fragment kodu, do którego możemy się odwoływać wielokrotnie. Aby zbudować funkcję używamy następującej składni

.. code-block:: python

  def nazwa_funkcji(<lista_argumentow>):
      CIAŁO_FUNKCJI
      <return obiekt>

Obiekty, które widzicie w nawiasach ostrych (``lista_argumentow`` i ``return obiekt``) nie są obowiązkowe i można je pominąć budując funkcję. Zwykle jednak funkcja będzie przekształcać własnie listę argumentów (**dane wejściowe**) w ów ``obiekt`` który później będzie zwracać (**dane wyjściowe**) instrukcją **return**. Umówmy się, że na potrzeby tego kursu będziemy budować funkcje, które zawsze będą zawierały słowo kluczowe **return**.

Spróbujmy przepisać program obliczający szansę na 5 wygranych z użyciem funkcji obliczającej silnię. Daną wejściową będzie liczba ``n`` a wyjściową właśnie wartość silni.

.. activecode:: pl_main_0903
    :caption: 5 wygranych rzutów monetą na 10 prób z funkcją
    :enabledownload:

    def silnia(n):
        s = 1
        for liczba in range(1, n + 1):
            s = s * liczba
        return s

    silnia10 = silnia(10)
    silnia5 = silnia(5)
    p5 = (silnia10 / (silnia5 * silnia5)) * (0.5 ** 5) * ((1-0.5) ** (10-5))
    print('Szansa na pięciokrotną wygraną w 10 rzutach to {:.1f}%'.format(p5 * 100))

Nie jest to co prawda operator, ale użycie jej jest równie proste - musimy wywołać jej nazwę (tutaj to ``silnia``), podać żądaną listę argumentów (tu jest tylko jeden - ``n``) i przechwycić wartość zwracaną za pomocą przypisania

.. code-block:: python

  wynik = silnia(10)

Możemy pójść też krok dalej i zamiast obliczać szansę jak powyżej, możemy też napisać funkcję która by taką szansę obliczała. Możemy ją nawet nazwać tak, by nie zostawiała żadnych wątpliwości. Raz zaprogramowaną funkcję ``silnia`` możemy użyć ile razy chcemy

.. activecode:: pl_main_0904
    :caption: 5 wygranych rzutów monetą na 10 prób z funkcjami
    :enabledownload:

    def silnia(n):
        s = 1
        for liczba in range(1, n + 1):
            s = s * liczba
        return s

    def szansa_na_k_wygranych_na_n_rzutow_moneta(k, n):
        silnia_k = silnia(k)
        silnia_n = silnia(n)
        silnia_nk = silnia(n-k)
        szansa = (silnia_n / (silnia_k * silnia_nk)) * (0.5 ** k) * (0.5 ** (n-k))
        return szansa

    print('Szansa na pięciokrotną wygraną w 10 rzutach to {:.1f}%'.format(szansa_na_k_wygranych_na_n_rzutow_moneta(5, 10) * 100))

To teraz ćwiczenie...

.. topic:: Ćwiczenie

  Spróbuj zaprogramować funkcję, która będzie obliczać szansę na \\(k\\) wygranych w \\(n\\) próbach dla dowolnej gry, nie tylko sprawiedliwego rzutu monetą, gdzie orzeł i reszka wypada równie często, ale dla gier, gdzie tak nie musi być (np. gdy rzucamy sfałszowaną monetą). Oznacza to, że musimy napisać funkcję uwzględniajacą różne wartości zmiennej :math:`p \in [0, 1]` w pierwszym wzorze tej lekcji, nie tylko 0.5. W takim przypadku funkcja musi mieć listę 3 argumentów ``k, n, p``. Możesz wykorzystać powyższy *ActiveCode* lub też zaprogramować funkcję w swoim ulubionym środowisku programistycznym.
