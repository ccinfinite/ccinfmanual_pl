.. _petla_while:

***************
Pętla ``while``
***************

Czasem musimy wykonać pewien ciąg instrukcji wielokrotnie, może nawet czasem w nieskończoność. W językach programowania w takich przypadkach posługujemy się pętlami. Język Python posiada dwie takie instrukcje, działające na nieco innych zasadach.


.. index:: while, pętla

Najbardziej uniwersalną konstrukcją języka Python, umożliwającą wykonywanie
bloku instrukcji wielokrotnie jest instrukcja ``while``. Podobnie jak
rozgałęzienie bazuje ona na warunku logicznym.

.. code-block:: python

    while WARUNEK:
        BLOK_WHILE

Tak długo jak długo spełniony jest warunek (jest równy ``True``), tak długo wykonywany będzie blok instrukcji ``BLOK_WHILE``. Jak widać, znów blok instrukcj rozpoczyna się po dwukropku. OK, pora na prosty przykład operacji cyklicznej.
Aby zwrócić liczby od 1 do 5 podniesione do drugiej potęgi możemy wykorzystać pojęcie pętli

.. activecode:: pl_main_0701
    :caption: kwadraty liczb od 1 do 5

    liczba = 1
    while liczba <= 5:
        kwadrat_liczba = liczba ** 2
        print('{}^2 = {}'.format(liczba, kwadrat_liczba))
        liczba = liczba + 1

Ostatnia linia (numer 5) jest bardzo istotna. Jeżeli jej zapomnimy, co zdarza się nadspodziewanie często, pętla wykonywać się będzie w nieskończoność.
Poniżej znajdziecie tabelę, w której podane są, krok po kroku, wszystkie wartości zmiennej ``liczba`` i jej kwadratu, a także wynik testu ``liczba >= 5``. Zauważcie, że zmienna ``kwadrat_liczba`` nie zostanie obliczona dla ``liczba = 6``.

+------+--------------+-----------+
|liczba|kwadrat_liczba|liczba <= 5|
+======+==============+===========+
|  1   |      1       |    True   |
+------+--------------+-----------+
|  2   |      4       |    True   |
+------+--------------+-----------+
|  3   |      9       |    True   |
+------+--------------+-----------+
|  4   |     16       |    True   |
+------+--------------+-----------+
|  5   |     25       |    True   |
+------+--------------+-----------+
|  6   |              |   False   |
+------+--------------+-----------+

Znając już dwie konstrukcje warunkowe ``if`` oraz ``while``, możemy już konstruować dowolnie skomplikowane problemy, możemy rozwiązać dowolne zadanie programistyczne. Nie zawsze będzie to łatwe, czasem może się wydawać niewykonalne, ale zwykle da się to zrobić. Spróbujemy wypisać teraz liczby podzielne przez 2 i 3, ale te większe od 33 i jednocześnie mniejsze od 67.

.. activecode:: pl_main_0702
    :caption: liczby podzielne przez A i B w granicy od start do stop

    start, stop = 33, 67
    A, B = 2, 3

    num = start
    while num <= stop:
        if (num % A == 0) and (num % B == 0):
            print('liczba {} jest podzielna przez {} i {}'.format(num, A, B))
        num = num + 1

Jak widzicie powyżej, przy instrukcji warunkowej oba warunki ujeliśmy w nawiasy okrągłe. W taki sposób możemy *grupować* obliczenia czy instrukcje, które powinny być wykonane oddzielnie od siebie, a nie jesteśmy pewni czy zapis bez nawiasów nam to zapewni. W tym konkretnym przypadku pozbycie się nawiasów niczego nie zmieni, gdyż iloczyn logiczny ``and`` ma wśród operatorów `jeden z najniższych priorytetów <https://docs.python.org/3/reference/expressions.html#operator-precedence>`_. Niezależnie od wagi operacji, nawiasy często znacząco podnoszą czytelność kodu i zachęcamy do ich (rozsądnego!) stosowania.

.. topic:: Ćwiczenie

    Zmodyfikuj powyższy program tak, by obliczyć **sumę** wszystkich liczb podzielnych przez 2 i 3, większych od 33 i mniejszych od 67.

.. index:: while - else

Podobnie jak dla rozgałęzień w pętli ``while`` możemy użyć instrukcji ``else``.

.. code-block:: python

    while WARUNEK:
        BLOK_WHILE
    else:
        BLOK_ELSE

i rozumieć możemy ją dokładnie tak jak znaną już konstrukcję ``if - else``. W
momencie, gdy ``WARUNEK`` nie będzie spełniony, uruchomi się blok instrukcji
pod ``else``. Mamy dwie takie możliwości:

* pętla ``while`` wykona się do końca i po niej wywoła się blok ``else``

  .. code-block:: python

      i = 1
      while i < 8:
          print(2 ** i)
          i += 1
      else:
          print('koniec dzialania petli')

* pętla ``while`` nie wykona się w ogóle (warunek nie będzie spełniony),
  wykona się natomiast blok ``else``

  .. code-block:: python

      i = 10
      while i < 8:
          print(2 ** i)
          i += 1
      else:
          print('petla while nie wywola sie wcale')


.. index:: break, continue

Wyrażenia ``break`` oraz ``continue``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Jeżeli oczekujemy przerwania wykonywania pętli w jakiejś chwili możemy użyć słowa kluczowego ``break``.

.. activecode:: pl_main_0703
    :caption: break

    j = 10
    while True:
        if j > 23:
            print('stop: j={} jest wieksze od 23'.format(j))
            break
        j += 3

.. note:: **while True**

  Konstrukcja taka wykorzystywana jest, gdy chcemy zbudować niekończącą się pętlę. Jako, że warunek ``True`` będzie zawsze prawdziwy, musimy użyć innego sposobu by taką pętlę zatrzymać - możemy np. posłużyć się instrukcją ``break`` tak jak w przykładzie powyżej. Możemy też wywołać :ref:`podprogram <funkcja>`, który przejmie sterownaie programem. O podprogramach opowiemy już niedługo.

Dokładnie odwrotne zachowanie ma instrukcja ``continue``. W momencie
napotkania takowej przestaną wykonywać się instrukcje w bloku pętli i nastąpi
kolejny obrót pętli, niezależnie od stanu programu.

.. activecode:: pl_main_0704
    :caption: continue (nie wydrukuje się liczba -3)

    j = -5
    while j <= 5:
        j += 1
        if j == -3:
            print('pomijamy {}'.format(j))
            continue
        print('szescian {} to {}'.format(j, j ** 3))

.. topic:: Ćwiczenie

  Czasem, gdy uruchamiamy program komputerowy, będzie zadawał nam różne pytania, z reguły potrzebne by go poprawnie skonfigurować. Czasem odpowiedź ogranicza się do decycji "tak" lub "nie", więc jak napiszemy "ok" to program nie będzie wiedział co mamy na myśli - będziemy zmuszeni podać dokładnie albo słowo "tak" albo słowo "nie", a jeżeli podamy inne, to program wróci do tego samego pytania i będzie tak długo pytał aż nie podamy jednego z tych słow. Posługując się nieskończoną pętlą oraz instrukcją ``break`` napisz taki program. Poniżej prezentujemy szkielet tego programu. W miejsce komentarzy wpisz swój kod.

.. activecode:: pl_main_0705
    :caption: ćwiczenie

    while True:
        odp = input('Wpisz tak lub nie: ')
        # jeżeli tak: wydrukuj 'Zgoda!' i zakończ pętlę (program)
        # jeżeli nie: wydrukuj 'Brak zgody!' i zakończ pętlę (program)
        # jeżeli co innego: zapytaj ponownie o wprowadzenie tak lub nie
        break
