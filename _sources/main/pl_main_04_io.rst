.. _io:

******************************
Instrukcje wejścia i wyjścia
******************************

Operacje wejścia i wyjścia odpowiadają za odpowiednio odczyt oraz zapis danych.
Podstawowymi operacjami jakie będą nas interesować związane są z prostą komunikacją z użytkownikiem. W sumie mamy na myśli wyświetlanie informacji na ekranie oraz odczyt znaków podawanych z klawiatury. Służą do tego odpowiednio funkcje ``print`` oraz ``input``.

.. index:: print, formatowanie proste print

Funkcja ``print``
~~~~~~~~~~~~~~~~~

Aby zmusić funkcję ``print`` by coś nam wydrukowała na ekranie, wystarczy podać po jej nazwie, w nawiasach okrągłych podać obiekt, który chcemy zobaczyć. Może to być jakaś wartość podana jawnie, może być też zmienna - bez znaczenia.

.. code-block:: python

    >>> print(13)
    13
    >>> z = 13
    >>> print(z)
    13

Takie wyświetlanie zawartości zminnych nazywane jest formatowaniem prostym. Z jego pomocą możemy wyświetlać szereg zmiennych/wartości, wystarczy że podamy ich listę po przecinku, jeden po drugim.

.. activecode:: pl_main_0401
    :caption: print - formatowanie proste

    imie = 'Ala'
    wiek = 13
    print(imie, "ma", wiek, "lat.")

.. topic:: Ćwiczenie

    Korzystając z powyższgo przykładu, spróbuj dopisać jeszcze kilka zmiennych i wydrukować za pomocą ``print`` informacje o tym, że ``'Ala ma 13 lat, mieszka w mieście Katowice, a jej ulubiony sport to koszykówka.'``.


Opcjonalne argumenty funkcji ``print``
--------------------------------------

.. index:: print argument sep

Domyślnym znakiem, który oddziela od siebie elementy listy wartości drukowanych przez funkcję ``print`` jest spacja. Możecie to zaobserwować w przykładzie powyżej. Znak ten możemy jednak dowolnie zmieniać. Służy do tego argument ``sep``. Aby zmienić domyślną spację na znak ``+``, wystarczy napisać

.. code-block:: python

    >>> print('Ala', 'ma', '13', 'lat.', sep='+')
    'Ala+ma+13+lat.'

Aby zamienić ``+`` na ``---`` wystarczy podmienić podany separator na nowy, taki jaki chcecie.

.. showeval:: showEval_print_sep
   :trace_mode: false

   print('Ala', 'ma', '13', 'lat.', sep='+')
   ~~~~
   print('Ala', 'ma', '13', 'lat.', sep='{{+}}{{---}}')


Proste prawda? Spróbujcie sami. Poniżej macie *ActiveCode*. Zamieńcie ``'+'`` na inny separator, np: ``' @('_')@ '``, zobaczycie jak to działa.

.. activecode:: pl_main_0402
    :caption: print - sep

    print('Ala', 'ma', '13', 'lat.', sep='+')


.. index:: print argument end

Podobnie jak z separatorem, możemy samodzielnie ustalić w jaki sposób ``print`` zakończy wyświetlanie listy argumentów. W tym przypodku korzystamy z argumentu ``end``. Domyślnie ``print`` łamie linię i przechodzi do nowego wiesza. Oznacza to, że ``end`` ustawiony jest na polecenie *złam linię*, które zapisujemy ``end='\n'``. Znaki ``\n`` to znaki specjalne oznaczające właśnie łamanie linii. Jest ich nieco więcej, jeżeli jesteście zainteresowani odsyłamy do części traktującej o :ref:`manipulacji stringami <pl_lingua_intro>`.

Gdy podamy 4 zmienne w 4 wywołaniach funkcji ``print``, zobaczymy je w 4 osobnych liniach

.. code-block:: python

    print('Ala')
    print('ma')
    print('13')
    print('lat.')
    Ala
    ma
    13
    lat.

Jeżeli natomiast ustawimy argument ``end=' '`` (lub cokolwiek innego, podobnie jak poprzednio dla ``sep``), zobaczymy że print nie będzie przechodził do nowej linii po wyświetleniu ciągu znaków, ale zamiast ją łamać umieści tam znak podany do zmiennej ``end``. Zwróćcie uwagę, że od ostatniego ``print``-a chcemy żeby jednak łamał linię.

.. activecode:: pl_main_0403
    :caption: print - end

    print('Ala', end=' ')
    print('ma', end='... ')
    print('13', end="!!! ")
    print('lat.', end='\n')

.. index:: print argument flush, print argument file

Na koniec wspomnimy tylko, że istnieją jeszcze dwa inne opcjonalne argumenty: ``file`` i ``flush``. Wykorzystywane są one podczas zapisu zawartości zmiennych nie na ekran ale do pliku, do którego uchwyt podajemy do zmiennej ``file``. Drugi argumnet ``flush`` jest zmienną typu logicznego i dzięki niej możemy zmusić funkcję ``print`` do wypisywania zawartości zmiennych od razu (gdy ustawimy ją na ``True``) lub pozwolić na ich buforowanie (``False``). Nie przejmujmy się tym na razie, gdyż do niczego na razie nie będzie to potrzebne.
O obsłudze plików też będziemy mówić przy innej okazji.


Instrukcja ``input``
~~~~~~~~~~~~~~~~~~~~

Czasem chcemy wprowadzić nieco interakcji do naszych programów, zapytać o coś użytkownika i oczekiwać odpowiedzi (tak/nie). Aby dynamicznie przekazać jakąś wartość, niezależnie czy będzie to liczba, czy może jakiś tekst, wykorzystujemy funkcję ``input(komunikat)``. Ów ``komunikat`` to ciąg znaków który zobaczy użytkownik przed podaniem wartości. Zobaczmy jak to działa

.. activecode:: pl_main_0404
    :caption: input

    imie = input('Jak masz na imię? ')
    wiek = input('Ile masz lat? ')
    print(imie, 'ma', wiek, 'lat.')


Dane wprowadzone przez użytkownika nie są interpretowane, a typ wielkości wynikowej, tej zapisywanej w zmiennej, to zawsze łańcuch znaków (``str``).

Czasem jednak, wartości podawane przez użytkownika są liczbowe i właśnie jak liczby chcielibyśmy je przechwytywać do zmiennych, po to, by coś z nich móc obliczyć. Aby to umożliwić, wystarczy zrzutować jawnie to, co pobiera funkcja ```input`` na interesujący nas typ. Aby zamienić pobrane dane na liczbę całkowitą musimy użyć funkcji ``int``. W przypadku liczb zmiennoprzecinkowych użyjemy funkcji ``float``.

.. activecode:: pl_main_0405
    :caption: input, int i float

    koty = int(input('Ile masz kotów? '))
    print('Masz w domu', 4 * koty, 'futrzaste nogi!')

    wzrost = float(input('Ile masz wzrostu (w metrach)? '))
    print('Masz {} centymetrów!'.format(wzrost * 100))

.. topic:: Ćwiczenie

    Korzystając z poprzedniego ćwiczenia oraz powyższego przykładu, spróbujcie napisać krótki program, który zapyta użytkownika o kilka dodatkowych informacji. Oprócz imienia i wieku, podobnie jak wcześniej zapytajcie o miasto w którym mieszka oraz ulubiony sport. Na koniec wydrukujcie za pomocą ``print`` te informacje w jak najbardziej czytelnej postaci. Możesz wykorzystać poprzednie formatowanie oraz powyższy *ActiveCode* do zaprogramowania rozwiązania.
