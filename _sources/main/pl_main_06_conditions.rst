.. _rozgalezienia:

********************
Rozgałęzienia
********************

.. index:: bool, typ logiczny, True, False

Mówiliśmy już o logicznym typie danych ``bool``. W ramach tego typu możemy wyróżnić dokładnie dwie wartości: ``True`` (prawda) bądź ``False`` (fałsz).  W wielu językach programowania przyjęto liczbę ``0`` jako odpowiednik fałszu i ``1`` (lub w ogólności każdą inną liczbę) jako odpowiednik prawdy.
Dla tego typu mamy zdefiniowane standardowe operatory logiczne.

.. index:: rzutowanie jawne

Nazwa typu logicznego to jednocześnie nazwa funkcji ``bool(obj)``, która umożliwia zmianę (rzutowanie) zmiennej ``obj`` na typ logiczny

.. activecode:: pl_main_0601
   :caption: bool, typ logiczny

   print("bool(1):", bool(1))
   print("bool(-3):", bool(-3))
   print("bool(0):", bool(0))
   print("bool(None):", bool(None))
   print("bool([1, 2, 3]):", bool([1, 2, 3]))
   print("bool([]):", bool([]))
   print("bool('Ala ma kota'):", bool('Ala ma kota'))
   print("bool(''):", bool(''))


Z poprzednich lekcji wiemy, że podstawowe operatory logiczne to ``and`` (logiczne i), ``or`` (logiczne lub) oraz ``not`` (logiczna negacja).  Znajdziecie też tam omównienie operatorów porównania (``<``, ``==``, itd.), w wyniku działania których również dostajemy literały typu ``bool``.
Ten właśnie typ jest podstawą do konstrukcji rozgałęzień programów.

**Rozgałęzienie** to takie miejsce w programie
komputerowym, w którym na podstawie jakiegoś zdania logicznego decydujemy co
program ma dalej wykonywać. W najprostszym przypadku program rozdziela się na
dwie części.

Instrukcja ``if``
=================

.. index:: if, rozgałęzienie

W języku Python instrukcją rozgałęziającą program jest ``if``, co tłumaczymy
jako 'jeżeli'. Zastanówmy się nad takim problemem

  Jeżeli wartość zmiennej ``x`` jest ujemna to zmień ją na liczbę dodatnią, a w przeciwnym przypadku nie rób nic.

Możemy takie matematyczne zadanie *przetłumaczyć* na język programowania

  Jeżeli zmienna ``x`` jest mniejsza od liczby ``0`` to zmień ją na liczbę o przeciwnym znaku, a w przeciwnym przypadku nie rób nic.

Jak widać, dostajemy dwie możliwości - albo liczba ``x`` jest ujemna albo nie jest. Mamy do czynienia z rozgałęzieniem właśnie. Aby napisać program wykonujący takie zadanie, musimy wykorzystać instrukcję warunkową ``if``

.. code-block:: python

    if x < 0:
        x = -x

.. topic:: Ćwiczenie

    Nie jest to jedyna możliwość rozwiązania problemu zmiany liczb ujemnych na dodatnie. Jak jeszcze można zmienić liczby z ujemnych na dodatnie? Spróbujcie znaleźć jeszcze 2 inne sposoby i zapiszcie je w języku Python.

..
  x *= -1
  x -= 2 * x
  x = abs(x)
  x = (x ** 2) ** 0.5

.. index:: wcięcie, blok kodu

.. note:: **Wcięcia**

    Aby zaznaczyć, że dana część kodu należy do danej gałęzi instrukcji warunkowej ``if`` musimy w języku Python zastosować wcięcie, czyli przesująć część kodu o kilka spacji w prawo. Zwyczajowo stosuje się dokładnie 4 spacje by rozróżnić dany **blok kodu**. Na początku nauki programowania może się to wydawać zbędne i uciążliwe, na szczęście większość edytorów tekstu robi to za nas automatycznie. Wystarczy rozpocząć dany blok kodu za pomocą **dwukropka** ``:``, by edytor automatycznie przesunął kolejną linię kodu o 4 spacje. Jeżeli bardzo chcecie możecie używać dowolnej ilości spacji (a nawet tabulacji) by wciąć blok kodu, ale najlepsi programiści Pythona używają czterech.


Pełny schemat rozgałęzienia w języku Python ma postać:

.. code-block:: python

    if WARUNEK:
        BLOK_INSTRUKCJI_IF
    elif WARUNEK_1:
        BLOK_1
    elif WARUNEK_2:
        BLOK_2
    ...
    else:
        BLOK_ELSE

Widzimy dwie nowe instrukcje: ``elif`` oraz ``else``. Pierwszą moża odczytać
jako *else if* i należy rozumieć *w przypadku gdy*. Ostatnią instrukcją jest
``else``, która oznacza *w każdym innym przypadku*. Obowiązkową konstrukcją
jest zastosowanie słowa kluczowego ``if``, wszystkie inne słowa są opcjonalne.
Służą one nieco bardziej skomplikowanym rozgałęzieniom.  Python będzie
sprawdzał powyższe warunki po kolei, jeden po drugim. Jeżeli napotka na
**warunek** który będzie prawdą, to reszta warunków nie będzie już sprawdzana.
Co więcej - bloki instrukcji nie będą nawet interpretowane. Sprawdzana będzie
tylko poprawność składni.

Popatrzmy na przykład. Przetestujemy,  czy zmienna ``liczba`` wskazuje na
liczbę parzystą.

.. activecode:: pl_main_0602
   :caption: parzystość

   liczba = 12
   if liczba % 2 == 0:
       p_czy_np = ''
   else:
       p_czy_np = 'nie'
   print('liczba jest {}parzysta'.format(p_czy_np))

Oczywiście w powyższym przykładzie zobaczymy, że 'liczba jest parzysta'. Pobawcie się powyższym kodem i sprawdźcie dla jakich liczb otrzymujemy informację o liczbach parzystych, a dla jakich nie.
Parzystość liczby oznacza, że jest ona
podzielna przez 2 bez reszty, co oznacza, że operacja obliczania reszty z dzielenia za pomocą operatora ``%`` powinna zwrócić zero. Właśnie ten fakt testujemy w pierwszym rozgałęzieniu ``if liczba % 2 == 0:``. Na pytanie o parzystość możemy odpowiedzieć "tak" lub "nie", nie ma innej możliwości. W takim wypadku, jeżeli liczba okazała by się nie być parzysta (omawiany warunek byłby równy ``False``) możemy posłużyć się opcją "w każdym innym przypadku", bo jest to jedyny inny przypadek. Dlatego też wykorzystaliśmy konstrukcję ``if - else``. Jest to niezwykle częsta sytuacja.

Aby pokazać nieco bardziej skomplikowane rozgałęzienie napiszemy program, który będzie nas informował ile dni ma dany miesiąc. Aby wyróżnić trzy możliwe odpowiedzi (31, 30 i 28/29), powinniśmy użyć konstrukcji ``if-elif-else``. Dodatkowo możemy zabezpieczyć się przed złą odpowiedzią, jeżeli użytkownik naszego programu poda numer miesiąca spoza zakresu $[1, 12]$. W tym wypadku będziemy potrzebować dwóch instrukcji ``elif``.

.. activecode:: pl_main_0603
   :caption: Ilość dni w miesiącu.

   dni_31 = [1, 3, 5, 7, 8, 10, 12]
   dni_30 = [4, 6, 9, 11]
   miesiac = int(input('Podaj numer miesiąca (od 1 do 12): '))
   if miesiac == 2:
       print('{} miesiąc ma 28 lub 29 dni'.format(miesiac))
   elif miesiac in dni_31:
       print('{} miesiąc ma 31 dni'.format(miesiac))
   elif miesiac in dni_30:
       print('{} miesiąc ma 30 dni'.format(miesiac))
   else:
       print('Rok ma 12 miesięcy')


.. topic:: Ćwiczenie

    Zazwyczaj pytając o miesiąc mamy na myśli jego nazwę, nie kolejność w roku. Spróbuj przepisać tak powyższy program, by pytał użytkownika o nazwę miesiąca i na jej podstawie zwracał informację o liczbie dni. Prawdopodobnie lepiej będzie zaprogramować rozwiązanie w jakimś innym środowisku, nie tu w *ActiveCode*, aby było prościej skorzystaj z :download:`tego skryptu <pl_main_ch06e03.py>`.
