.. zmienne:

*********
Zmienne
*********

.. index:: literał, operator, konstrukcje prymitywne

Zaczniemy od omówienia najprostszych konstrukcji z których zbudowane są praktycznie wszystkie języki programowania.
Takie *konstrukcje prymitywne* możemy podzielić na **literały**, czyli pewnego rodzaju wartości jakimi operuje programista oraz **operatory** służące do manipulowania owymi literałami.

Chyba najprostszym przykładem będzie dodawanie dwóch liczb. Każdy z nas zna wynik dodawania :math:`2 + 2`.
Aby zobaczyć wynik dodawania w poniższym okienku musimy opakować działanie funkcją ``print``.
Wypróbujcie poniższy kod, następnie podmieńcie liczby oraz działanie dodawania (``+``) na jakieś inne, które pamiętacie z lekcji matematyki.

.. activecode:: pl_main_0201
   :coach:
   :caption: Działania na liczbach

   print(2 + 2)


.. index:: zmienna, operator przypisania

Wynik działania możemy też zapamiętać. Właśnie w tym celu w językach programowania tworzymy **zmienne**. Zmienną tworzy się bardzo prosto - musimy podać jej nazwę, następnie znak równości (**operator przypisania**) i wartość, jaką chcemy do tej zmiennej przypisać, np:

.. code:: Python

   zmienna = 2

W poniższym oknie możesz wypróbować tworzenie zmiennych. W tym przypadku do zmiennej ``wynik_dodawania`` od razu przypisujemy wynik dodawania dwóch liczb ``2 + 2``, a do zmiennej ``wynik_odejmowania`` różnicę ``2 - 2``.

.. activecode:: pl_main_0202
   :coach:
   :caption: Tworzenie zmiennej

   wynik_dodawania = 2 + 2
   print('Wynik dodawania:', wynik_dodawania)
   wynik_odejmowania = 2 - 2
   print('Wynik odejmowania:', wynik_odejmowania)


.. topic:: Ćwiczenie

    W powyższej komórce dopisz kolejne linijki programu, w których obliczysz i wydrukujesz (za pomocą ``print``) iloczyn i iloraz dwóch dowolnych liczb. Możesz wciąż używać dwójek, ale może pokusisz się na coś nieco bardziej ekscytującego...


Podobnie jak w zadaniach z matematyki, odpowiednie obliczenia możemy grupować za pomocą nawiasów. Robimy to z różnych powodów - by ustrzec się przed pomyłką lub by sprecyzować kolejność działań. Jeżeli chcielibyśmy dodać do siebie podwojoną wartość dwóch liczb 3 i 4, to wystarczy napisać ``2 * 3 + 2 * 4`` lub

.. activecode:: pl_main_0203
   :coach:
   :caption: Nawiasy

    wynik = 2 * (3 + 4)
    print(wynik)


.. index:: typ danych

W ogólności nawiasy służą do grupowania operacji na dowolnych zmiennych o dowolnych *typach*, nie jest to ograniczone do działań na liczbach. Umożliwia to kontrolę nad kolejnością wykonywania instrukcji. W większości języków programowania możemy używać wartości (literałów) o różnych własnościach. Wartość i sposób zapisu będzie nam definiował **typ** danych.

.. topic:: Ćwiczenie

    Będąc na zakupach znalazłeś dwie pary butów, które chcesz kupić. Czerwone kosztują 189 zł, a niebieskie 259 zł, ale jest na nie 25% obniżka. Za zakupy powyżej 399 zł dostaniesz dodatkową bonifikatę 20%!

    Która para butów będzie kosztowała więcej? Obliczmy cenę niebieskich butów po obniżce, potem możesz zaznaczyć właściwą odpowiedź poniżej.

    .. activecode:: pl_main_0204
       :coach:
       :caption: Nawiasy - ćwiczenie

       cena_niebieskie = 259
       obnizka = (100 - 25) / 100
       nowa_cena = cena_niebieskie * obnizka
       print(nowa_cena)

    .. clickablearea:: pl_main_cw0201
      :question: Która para butów będzie kosztowała więcej?
      :table:
      :correct: 1,2
      :incorrect: 1,1
      :feedback: Uruchom działania w powyższym *ActiveCode* i sprawdź wynik

      +-----------+-------------+
      | czerwone  | niebieskie  |
      +-----------+-------------+

    .. fillintheblank:: pl_main_cw0202
       :casei:

       Ile będą kosztowały obie pary?

       -   :383.25: Doskonale!
           :x: Niepoprawnie. Spróbuj obliczyć sumę w okienku wyżej.

    .. clickablearea:: pl_main_cw0203
          :question: Czy jeżeli zdecydujesz kupić się obie pary to uda się uzyskać dodatkową zniżkę?
          :table:
          :correct: 1,2
          :incorrect: 1,1
          :feedback: Wróć do komórki *ActiveCode* i dodaj do siebie obie liczby

          +------+------+
          | tak  | nie  |
          +------+------+


Podstawowe typy danych
======================

.. index:: typ int, typ float

Typy liczbowe
~~~~~~~~~~~~~

We wcześniejszych przykładach poznaliśmy już kilka typów danych. Literały takie jak ``2`` lub ``2.0`` oznaczają po prostu liczbę 2 (literał liczbowy o wartości 2), ale mają dwa różne typy. Literał ``2`` ma wartość 2 i przypisany typ całkowity (integer, ``int``). Ten drugi, ``2.0``, ma również wartość 2, ale typ zmiennoprzecinkowy (``float``). Oba możemy traktować po prostu jak liczbę 2, ale ich sposób zapisu w pamięci komputera będzie inny. Więcej o tym możecie przeczytać w części omawiającej :ref:`zagadnienia numeryczne <wszystko_jest_liczba>`.

.. index:: typ bool, True, False

Typ logiczny
~~~~~~~~~~~~

Poszczególne typy danych różnią się między sobą zbiorami dozwolonych
wartości oraz zestawem dozwolonych operacji, które można na nich wykonać.
Jednym z najczęściej wykorzystywanych typów danych jest typ logiczny (``bool``).
Może on przyjmować wartości ``True`` oznaczający logiczną prawdę lub ``False`` oznaczający logiczny fałsz. Wiemy na przykład, że liczba 2 jest większa od liczby 1. Z tego powodu, pisząc :math:`2 > 1`, wiemy, że wyrażenie jest prawdziwe. Taką samą operację możemy zaprogramować

.. code:: Python

    >>> 2 > 1
    True

Co więcej, wynik takiego porównania (owo ``True``) możemy przypisać do zmiennej

.. activecode:: pl_main_0205
    :coach:
    :caption: Typ logiczny

    print(2 > 1)
    prawda = 2 > 1
    print(prawda)

Jeżeli teraz wrócimy do zadania z zakupem butów, to zamiast samodzielnie oceniać, która para jest droższa, można było by rachunki i wnioskowanie pozostawić interpreterowi

.. activecode:: pl_main_0206
    :coach:
    :caption: Zadanie z butami

    buty_czerwone = 189
    buty_niebieskie = 259 * 0.75
    print(buty_czerwone > buty_niebieskie)


.. index:: operator porównania

Z typem logicznym związane są podstawowe operatory, którymi możemy porównywać zmienne i w wyniku takiego porównania otrzymać prawdę lub fałsz. Mamy operator większości ``>`` który już poznaliśmy ale i mniejszości ``<``. Możemy też sprawdzać czy zmienna jest większa lub równa (``>=``) oraz mniejsza lub równa (``<=``) innej zmiennej. Równość sprawdzamy za pomocą podwójnego znaku równości ``==``, co jest bodajże żródłem jednego z najczęstszych błędów początkujących programistów. Nierównośc testujemy za pomocą znaków ``!=``. W poniższej komórce *ActiveCode* możesz wypróbować działanie operatorów porównania według własnego pomysłu.

.. activecode:: pl_main_0207
    :coach:
    :caption: Operacje porównania

    print(100 > 10)

Operacje porównania mogą zostać wykonane na wszystkich obiektach. Wszystkie mają też ten sam priorytet, więc pod uwagę będą brane od lewej do prawej.
W języku Python możemy też porównywać czy dany obiekt (więcej o obiektach w rozdziale o :ref:`programowaniu obiektowym <oop>`) jest tożsamy z innym obiektem. Służą do tego operatory ``is`` oraz ``is not``. Nie będą one jednak zbytnio przydatne w tym kursie.


.. index:: typ NoneType, None

Typ ``NoneType``
~~~~~~~~~~~~~~~~

W języku Python istnieje też specjalny typ danch zwany ``NoneType``. W ramach tego typu znajdziemy tylko jedną wartość ``None``. Jest ona używana do wyrażania braku sensownej wartości, nieistnienia danej czy przechowywania *pustej* zawartości. Jest też wartością zwracaną z funkcji gdy pominiemy w niej słowo kluczowe ``return``, lub gdy po tym słowie nie podamy żadnej wartości. Więcej o funkcjach dowiesz się w :ref:`późniejszych rozdziałach <funkcje>` tego podręcznika.

.. index:: funkcja type

Żeby sprawdzić jakiego typu jest dany obiekt, można użyć funkcji ``type(obj)``,
przykładowo:

.. code-block:: python

    >>> type(3)
    <type 'int'>
    >>> type(7.5)
    <type 'float'>
    >>> type('a')
    <type 'str'>
