.. _oop:

***********************
Programowanie obiektowe
***********************

.. index:: klasa, class, instancja

To już ostatni rozdział tego podręcznika. Będziemy w nim mówić o obiektach. Biorąc pod uwagę, że w przypadku języka Python `wszystko jest obiektem <https://pl.wikibooks.org/wiki/Zanurkuj_w_Pythonie/Wszystko_jest_obiektem>`_, to chyba powinien być to rozdział pierwszy, ale na to już za późno.
Nie będziemy tutaj omawiać obiektów, klas i ich instancji bardzo wnikliwie, opowiemy tylko jak w prosty sposób zbudować **klasę** oraz jej **instancję**, będącą tej klasy użytecznym reprezentantem. Cytując za wspomnianą książką Wikibooks

    Wszystko jest obiektem w tym sensie, że może być przypisane do zmiennej albo stanowić argument funkcji.

Aby zbudować samodzielnie obiekt, który będzie nam reprezentował jakieś dane oraz będzie pozwalał na manipulacje tymi danymi (zmianę, wyodrębnianie istotnych informacji...) musimy zaprogramować klasę. W języku Python posługujemy się następującą konstrukcją

.. code-block:: python

  class NazwaKlasy:
    CIAŁO_KLASY

Klasy mogą reprezentować dowolne wymyślone przez nas obiekty. Możemy zbudować klasy opisujące kaczkę, gruszkę czy stolik do kawy. Bez znaczenia. Każdy z tych rzeczywistych "obiektów" posiada pewne fizyczne atrybuty, takie jak kolor czy waga, ale możemy też pomyśleć o atrybutach wyróżniających obiekty. Dla kaczki oraz gruszki może to być ich gatunek, dla stolika typ materiału z jakiego jest zrobiony.

Klasa definiuje stan i zachowania obiektów. Możemy sobie za jej pomocą opisać jakie dany obiekt ma własności oraz co z nim można zrobić.
Napiszmy klasę `Gruszka`, żeby zobrazować ideę. Gruszkę taką możemy opisać mówiąc o jej odmianie, kolorze (który zapewne wiąże się z odmianą), wadze, wielkości (ta wiąże się z wagą), smaku...

.. code-block:: python

    class Gruszka:
        '''
        Obiekt Gruszka opisuje nam wlasnosci gruszek.
        '''

        def __init__(self, odmiana, waga, smak):
            self.odmiana = odmiana
            self.waga = waga
            self.smak = smak

W ten sposób zbudowaliśmy klasę opisującą gruszki. Nie można nic ciekawego z tą klasą na razie zrobić, ale zbudowaliśmy klasę, będącą definicją **obiektu** który możemy stworzyć.
Możecie zauważyć, że zaraz pod definicją ``class Gruszka:`` umieściliśmy ciąg znaków ``'Obiekt Gruszka opisuje nam wlasnosci gruszek.'``. Podobnie jak w przypadku funkcji jest to ciąg reprezentujący dokumentację klasy (docstring). Dokumentacja jest zawsze w dobrym tonie.
Stwórzmy sobie obiekt bazując na tej klasie

.. activecode:: pl_main_1201
    :caption: Gruszka

    class Gruszka:
        '''
        Klasa Gruszka opisuje wlasnosci gruszek.
        '''

        def __init__(self, odmiana, waga, smak):
            self.odmiana = odmiana
            self.waga = waga
            self.smak = smak

    gruszka = Gruszka('General Leclerc', 130, 'kwaskowato-slodka')
    print(gruszka.odmiana)

Teraz zmienna ``gruszka`` przechowuje obiekt klasy ``Gruszka``.
Jeżeli potraktujem klasę jak wzorzec, który należy wypełnić, to obiekt ``gruszka`` będzie właśnie wypełnieniem takiego wzorca klasy ``Gruszka``, ale **z konkretnymi wartościami (argumentami)** podanymi do parametrów formalnych klasy. W tym przypadku

    ``odmiana`` <- 'General Leclerc'
    ``waga`` <- 130
    ``smak`` <- 'kwaskowato-slodka'

.. index:: instancja, atrybut instancji, atrybut klasy

Taki **skonkretyzowany wzorzec klasy** będziemy nazywać **instancją klasy** lub po prostu **instancją**. Instancja taka posiada swoje **atrybuty**: ``odmiana``, ``waga`` i ``smak``, które są w tym przypadku tożsame z **atrybutami klasy**. Możemy się do nich odwołać, tak jak do zwykłych zmiennych (bo też są zmiennymi) za pomocą *notacji z kropką*

.. code-block:: python

    >>> gruszka.odmiana
    'General Leclerc'

Tworząc nową instancję klasy za pomocą wywołania

.. code-block:: python

    gruszka = Gruszka('General Leclerc', 130, 'kwaskowato-slodka')

wypełniamy właśnie te parametry, które zadeklarowane zostały w definicji funkcji ``__init__(self, odmiana, waga, smak)``. Jest to bardzo niezwykła funkcja.

.. index:: init, __init__

Funkcja ``__init__``
====================

Właśnie owa niezwykła *funkcja specjalna* ``__init__`` służy do przechwytywania argumentów służących do instancjonowania klasy ``Gruszka``. Takich funkcji specjalnych jest więcej. Aby móc użyć argumentów w dowolnym miejscu w klasie (obiekcie) musimy przypisac je do **atrybutów**. Atrybuty można podzielić na atrybuty klasy i instancji. Te należące do instancji zaczynają się w klasie od przedrostka ``self.``.

.. index:: self

Nazwa ``self`` od której powinna rozpoczynać się każda funkcja występująca w ciele klasy, jest po prostu reprezenatcją instancji [#self]_. Innymi słowy w naszym przykładzie

.. code-block:: python

    gruszka = Gruszka('General Leclerc', 130, 'kwaskowato-slodka')

w środku klasy ``self`` zamienia się magicznie na ``gruszka``. Ten cały ``self`` nie musi nazwyać się ``self``, może nazywać się dowolnie. Jak by się nie nazywała (choć zalecamy jednak ``self``), będzie to **zmienna reprezentująca instancję w środku klasy**. Aby odwołać się do dowolnego atrybutu instancji, musimy w klasie poprzedzić ten atrybut właśnie zmienną ``self`` i odwołaniem poprzez kropkę.

.. index:: get, set, getter, setter

Gettery i settery
=================

Do atrybutów instancji (``odmiana, waga, smak``) nie powinniśmy odwoływać się bezpośrednio, niezależnie czy chcemy odczytać wartość tam stojącą, czy przypisać jakąś nową. Służą do tego specyficzne funkcje nazywane **getterami** (do czytania, nazwy ich zaczynają się od słowa ``get_``) i **setterami** (do zmiany wartości, ``set_``). W ramach klasy buduje się je dokłatnie tak jak zwykłe funkcje (od ``def``), z tą różnicą, że ich pierwszym argumentem znów powinien być ``self``.

.. activecode:: pl_main_1202
    :caption: Gruszka: gettery i settery

    class Gruszka:
        '''Klasa Gruszka opisuje wlasnosci gruszek.'''

        def __init__(self, odmiana, waga, smak):
            self.odmiana = odmiana
            self.waga = waga
            self.smak = smak

        def get_waga(self):
            return self.waga

        def get_odmiana(self):
            return self.odmiana

        def get_smak(self):
            return self.smak

        def set_waga(self, x):
            self.waga = x

        def set_odmiana(self, x):
            self.odmiana = x

        def set_smak(self, x):
            self.smak = x


    # instancjonowanie
    gruszka = Gruszka('General Leclerc', 330, 'kwaskowato-slodka')

    # odwołanie do metody get_smak() instancji gruszka
    print("Jaki smak ma gruszka?")
    print("Gruszka jest", gruszka.get_smak())

    # ustawienie zmiennej smak za pomoca set_smak(SMAK) dla instancji gruszka
    gruszka.set_smak('wybitnie slodka')

    print("Jaki smak ma gruszka?")
    print("Gruszka jest", gruszka.get_smak())

Jak widać funkcje ``get_smak()`` czy ``set_smak()`` wywołuje się podobnie jak zwykłe funkcje, ale stoi przed nią nazwa instancji (tutaj ``gruszka``). Proszę zauważyć, że pomimo, iż w ciele klasy ``Gruszka`` funkcja ``get_smak`` (linia 15) posiada 1 parametr formalny ``self``, to przy wywyłaniu tej funkcji (linie 42 czy 48) **nie podajemy do niej żadnego agrumentu**. Jest to typowa notacja (interfejs) do wywołania funkcji które przynależą do instancji. Funkcje takie nazwyamy **metodami**.

    **Metoda** jest funkcją związaną z instancją. Dzięki niej możemy wpływać na stan obiektów (instancji) - możemy je modyfikować.

W szczególności przyjmuje ona o 1 argument mniej od swojego odpowiednika (funkcji) w ciele klasy. Dzieje się tak, ponieważ do argumentu ``self`` trafia adres instancji  (``gruszka``) z której wywołujemy metodę. Pozostałe argumenty pojawiają się w wywołaniu tak jak w tradycyjnej funkcji.
Znów - magicznie nazwa instancji zostaje podstawiona w klasie pod zmienną ``self``.

.. index:: str, __str__

Klasa ``Kwadrat``
==================

Pora na nieco bardziej użyteczny przykład klasy. Zbudujemy relatywnie prostą klasę reprezentującą kwadraty.
Samą klasę nazwiemy ``Kwadrat``. Nazywanie klas z dużych liter, w konwencji
*CamelCase*, to dobra praktyka, zalecana przez programistów Pythona. Klasa będzie miała minimalną funkcjonalność: będzie obliczać pole i obwód kwadratów.

.. activecode:: pl_main_1203
    :caption: Klasa Kwadrat
    :enabledownload:

    class Kwadrat:
        """Klasa Kwadrat"""

        def __init__(self, a):
            self.set_bok(a)

        def get_bok(self):
            return self.bok

        def set_bok(self, var):
            if isinstance(var, (int, float)) and var > 0:
                self.bok = var
            else:
                print('Bok musi być liczbą > 0')

        def pole(self):
            return self.get_bok() ** 2

        def obwod(self):
            return 4 * self.get_bok()

        def __str__(self):
            return "{} o polu {} i obwodzie {}".format(
                self.__class__.__name__, self.pole(), self.obwod())


    k1 = Kwadrat(10)
    print(k1)
    k2 = Kwadrat(3.1415)
    print(k2)

Poeksperymentujcie z powyższą klasą. Zbudujcie inne kwadraty.
Jak już skończycie, przeczytajcie poniższy akapit. Powinien rozjaśnić co i jak
działa w takiej klasie. Na koniec czekają was ćwiczenia.

Aby opisać kwadrat potrzebujemy tylko jednego parametru - długości boku. Znając ten parametr możemy obliczyć dowolną wielkość opisującą kwadrat, jak jego pole czy obwód. Sam parametr formalny ``a``, który podajemy przy tworzeniu instancji. Tym razem, aby przekazać wartość parametru ``a`` funkcji ``__init__`` do atrybutu instancji o nazwie ``bok`` (widocznej w powyższym programie jako ``self.bok``) używamy od razu funkcji pomocniczej ``self.set_bok(a)``. Funkcja ta napisana jest defensywnie i nie pozwoli na podanie wartości innych niż dodatnie liczby o typie ``int`` lub ``float``. Jeżeli jednak użytkownik zdecyduje się podać jakąś inną wartość, zobaczy informację ``'Bok musi być liczbą > 0'`` a zmienna ``bok`` nie zostanie utworzona. Przy prawidłowych danych wejściowych, takich jak w dwóch przykładach instancji, zmienna ``bok`` przyjmie wartość podaną przez użytkownika i będziemy w stanie obliczyć pole i obwód kwadratu.

Obie funkcje użytkowe ``pole`` i ``obwod`` przyjmują tylko jeden parametr formalny ``self``, czyli odpowiednią instancję. Dzięki temu komplet atrybutów i metod stowarzyszonych z instancją będzie dostepny w tych funkcjach poprzez konstrukcję ``self.ATRYBUT``. Między innymi mamy do dyspozycji metodę ``get_bok`` zwracajacą długość boku kwadratu. Do niej musimy się odwołać, by móc obliczyć pole i obwód. Możemy oczywiście skorzystać bezpośrednio z wywołania ``self.bok``, będzie to po prostu inny sposób na obliczenie tego samego, bez wykorzystania idei getterów i setterów. Oba podejścia mają swoje plusy i minusy. Same funkcje są proste. Obliczając pole podnosimy długość boku do drugiej potęgi, a wyliczając obwód dodajemy do siebie długości wszystkich czterech jednakowych boków.

Nową rzeczą jest funkcja specjalna ``__str__``. Pamiętacie jeszcze :ref:`problem sklejania ciągów znaków <konkatenacja_znakow>`? Aby móc przekształcić dowolny obiekt na ciąg znaków musieliśmy użyć funkcji rzutującej ``str``. Funkcja ta wyszukuje właśnie w obiektach funkcji specjalnej ``__str__`` i ją wywołuje. Tak więc, budując taką funkcję tworzymy reprezentację naszego obiektu w typie ``str``. Co więcej - funkcja ``print`` również zwraca "na ekran" właśnie to co zaprogramujemy w funkcji ``__str__``. Tworząc klasę warto poświęcić chwilę na zaprogramowanie tej funkcji specjalnej. Dzięki temu możemy czytelnie opisać instancję obiektu, z którym przyjdzie nam później pracować.


.. topic:: Ćwiczenie

    Do klasy ``Kwadrat`` dopisz funkcję obliczającą długość przekątnej. Najprościej skorzystać ze wzoru :math:`d = a \sqrt{2}`.

.. topic:: Ćwiczenie

    Na bazie klasy ``Kwadrat`` zbuduj klasę ``Prostokąt``, pamiętając, że w ogólności prostokąty mają dwa różnej długości boki ``a`` i ``b``.


.. rubric:: Przypisy

.. [#self] tu ciut kłamiemy - nie każda funkcja musi zaczynać się of ``self``, ale na razie tak to zostawmy
