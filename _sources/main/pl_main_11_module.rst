.. _moduly:

.. index:: moduł

******
Moduły
******

.. index:: dekompozycja, abstrakcja

Tworząc program komputerowy, rozwiązując jakiś problem z jego pomocą zwykle będziemy tworzyć szereg funkcji, które to będą rozwiązywać daną część zagadnienia. W :ref:`lekcji o funkcjach <podprogramy>` napisaliśmy dwie funkcje - jedna obliczała silnię, a druga prawdopodobieństwo pewnej ilości zwycięstw w kilku rzutach monetą. Pierwsza z nich (``silnia``) była funkcją pomocniczą, która pozwoliła na redukcję kodu, poprawiła jego czytelność oraz ograniczyła możliwość popełnienia błędu - innymi słowy doprowadziła do swego rodzaju **dekompozycji** głównego zagadnienia na podproblemy (a raczej ich rozwiązania). Co więcej - uniezależniła przyszłe wykorzystanie funkcji obliczającej silnię od jej implementacji. Moglibyśmy usprawnić ten kod w jakiś sposób (np. rozpoczynać ``range`` od 2 a nie 1), co kompletnie nie wpłynęło by na sposób programowania z użyciem funkcji ``silnia``. Takie uniezależnienie implementacji od końcowego zastosowania nazywać będziemy **abstrakcją**. Właśnie dekompozycja i abstrakcja to dwa główne powody by używać funkcji.

Zagadnień kombinatoryki, które wymagają obliczeń silni jest zdecydowanie więcej. Nawet `rozdział podręcznika Wikibooks omawiający kombinatorykę <https://pl.wikibooks.org/wiki/Matematyka_dla_liceum/Rachunek_prawdopodobie%C5%84stwa/Elementy_kombinatoryki>`_ zaczyna się od definicji silni. W sumie, gdybyśmy pracowali nad zadaniami z kombinatoryki, dobrze by było mieć taką funkcję "pod ręką" i korzystać z niej bez konieczności samodzielnego definiowania za każdym razem funkcji obliczającej silnię. Jedną z możliwości jest skorzystanie z takiej funkcji zaprogramowanej w module ``math``. Aby z niej skorzystać, musimy zaimportować ją za pomocą komendy

.. code-block:: python

  from math import factorial

Od teraz możemy jej używać, tak jak używaliśmy funkcji napisanej przez nas. Możemy zatem obliczyć na ile różnych sposobów możemy ułożyć literki "abc". Skorzystamy ze wzoru na permutację zbioru 3 elementowego - odpowiedź to :math:`3!` (zerknij do podręcznika)

.. activecode:: pl_main_1101
   :caption: Permutacje i silnia

   from math import factorial
   wyraz = 'abc'
   odp = factorial(len(wyraz))
   print('{}! = {}'.format(len(wyraz), odp))

Z technicznego punktu widzenia moduł ``math`` to po prostu plik zawierający definicje i instrukcje języka Python. Nazwa pliku to nazwa modułu z dołączonym rozszerzeniem ``.py``.

.. index:: import, from

Korzystanie z modułów
=====================

Aby skorzystać z funkcji znajdujących się w modułach musimy zaimportować je do przestrzeni nazw.  Mamy dwie możliwości. Pierwszą znajdziecie powyżej

.. code-block:: python

  from moduł import obiekt
  # np
  from math import factorial

Dzięki tej metodzie będziemy mogli odwoływać się do ``obiektu`` (np. funkcji) w taki sposób jakby zdefiniowany był przez nas samych, w programie nad którym pracujemy. Zupełnie jak w przykładzie powyżej. Jeżeli chcemy, możemy zmienić nazwę importowanego obiektu na jaką chcemy używając konstrukcji ``from moduł import obiekt as inna_nazwa``

.. activecode:: pl_main_1102
   :caption: Importowanie funkcji z modułu

   from math import factorial as silnia
   wyraz = 'abc'
   odp = silnia(len(wyraz))
   print('{}! = {}'.format(len(wyraz), odp))


Jeżeli potrzebujemy kilku funkcji z jednego modułu, możemy je zaimportować podając ich listę

.. code-block:: python

  from moduł import obiekt1, obiekt2
  # np
  from math import factorial, gamma

Co umożliwi wykorzystanie obu funkcji w programie. Funkcja gamma to taka silnia uogólniona na liczby rzeczywiste. W poniższym przykładzie zaimportowaliśmy funkcję ``factorial`` ale nadaliśmy jej nazwę ``silnia`` oraz funkcję ``pow`` której nazwę zostawimy bez zmian.

.. activecode:: pl_main_1103
   :caption: Importowanie kilku funkcji z modułu

   from math import factorial as silnia, pow
   k, n = 5, 10
   print('Prawdopodobieństwo {} wygranych w {} rzutach monetą wynosi'.format(k, n), end=' ')
   print('{:.1f}%'.format(100 * silnia(n) * pow(0.5, 10) / silnia(k)**2))


Drugim sposobem jest zaimportowanie całego modułu i odwoływanie się do funkcji (i innych obiektów) w nim zawartych za pomocą *odwołania poprzez kropkę*. Tutaj też możemy zaimportować moduł korzystając z nazwy bardziej nam odpowiadającej.

.. code-block:: python

    import moduł
    import moduł as inna_nazwa

Jeszcze raz obliczymy na ile sposobów możemy poukładać literki "abc", ale odwołamy się do funkcji poprzez moduł i kropkę

.. activecode:: pl_main_1104
   :caption: Odwołanie poprzez kropkę

   import math
   wyraz = 'abc'
   odp = math.factorial(len(wyraz))
   print('{}! = {}'.format(len(wyraz), odp))


Nasz własny moduł
=================

Gdybyśmy chcieli zbudować swój własny moduł zawierający np: różne przydatne w kombinatoryce funkcje, wystarczy stworzyć plik o jakiejś rozsądnej nazwie i w tym pliku zawrzeć rzeczone funkcje. W naszym przypadku plik, więc i moduł, nazwiemy po prostu ``kombinatoryka.py``. W środku umieścimy funkcje, które zaprogramowaliśmy na poprzednich lekcjach.

.. literalinclude:: kombinatoryka.py
   :linenos:
   :emphasize-lines: 1, 17
   :caption: Ściągnij :download:`moduł kombinatoryka <kombinatoryka.py>`.

Aby móc skorzystać z nowo utworzonej biblioteki, trzeba ją zaimportować jedną z poznanych wyżej metod.

.. literalinclude:: pl_main_ch11e01.py
   :linenos:
   :emphasize-lines: 1
   :caption: Link do :download:`tego skryptu <pl_main_ch11e01.py>`.
