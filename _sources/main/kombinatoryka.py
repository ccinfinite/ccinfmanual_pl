def silnia(n):
    '''
    Funkcja obliczająca silnię.

    WEJŚCIE:
      n (INT) - liczba naturalna

    WYJŚCIE:
      INT: silnia z liczby n! = 1 * 2 * 3 * ... * n
    '''
    s = 1
    for liczba in range(2, n + 1):
      s = s * liczba
    return s


def szansa_na_k_wygranych_na_n_rzutow_moneta(k, n):
    '''
    Funkcja obliczająca szansę na k sukcesów w n próbach rzutu sprawiedliwą monetą

    p = n! / (k! * (n-k)!) * 0.5^n

    WEJŚCIE:
      k (INT) - ilość sukcesów
      n (INT) - ilość prób

    WYJŚCIE:
      FLOAT: szansę na k sukcesów w n próbach
    '''
    szansa = silnia(n) / silnia(k) / silnia(n-k) * 0.5 ** n
    return szansa
